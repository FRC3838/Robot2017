package frc.team3838.core.utils;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import static org.junit.Assert.*;



@RunWith(Parameterized.class)
public class MathUtils_ScaleTest
{

    private final double inputValue;
    private final double inputRangeMin;
    private final double inputRangeMax;
    private final double outputRangeMin;
    private final double outputRangeMax;
    
    private final double expected;


    public MathUtils_ScaleTest(double inputValue, double inputRangeMin, double inputRangeMax, double outputRangeMin, double outputRangeMax, double expected)
    {
        this.inputValue = inputValue;
        this.inputRangeMin = inputRangeMin;
        this.inputRangeMax = inputRangeMax;
        this.outputRangeMin = outputRangeMin;
        this.outputRangeMax = outputRangeMax;
        this.expected = expected;
    }


    @Parameters
    public static Collection<Object[]> data()
    {
        // @formatter:off
        return Arrays.asList(new Object[][] {
             /* index    input      In range            out range           expected*/
             /*   0 */  {0.5,       0,      1,          0,      100,        50},
             /*   1 */  {0.5,       0,      1,          0.2,    0.8,        0.5},
             /*   2 */  {0.25,      0,      1,          0.2,    0.8,        0.35},
             /*   3 */  {0.5,       0,      1,          0.2,    1.0,        0.6},
             /*   4 */  {0.25,      0,      1,          0.2,    1.0,        0.4},
             /*   5 */  {0.5,       0,      1,          0,      255,        127.5},
             /*   6 */  {0.5,      -1,      1,          0,      255,        191.25},
             /*   7 */  {-0.5,     -1,      1,          0,      255,        63.75},
             /*   8 */  {0.5,      -1,      1,         -0.5,    0.5,        0.25},
             /*   9 */  {-0.5,     -1,      1,         -0.5,    0.5,       -0.25},
             /*  10 */  {0.5,       0,      1,          0,      360,        180},
             /*  11 */  {0.25,      0,      1,          0,      360,        90},
        }); 
        // @formatter:on
    }


    @Test
    public void theTest()
    {
        final double actual = MathUtils.scaleRange(inputValue, inputRangeMin, inputRangeMax, outputRangeMin, outputRangeMax);
        final String msg = String.format("Wrong value for input of '%.3f' scaled from a range of %.3f \u2014 %.3f to %.3f \u2014 %.3f",
                                         inputValue, inputRangeMin, inputRangeMax, outputRangeMin, outputRangeMax);
        assertEquals(msg, expected, actual, 0.001);
    }
}


