package frc.team3838.core.hardware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.livewindow.LiveWindowSendable;
import edu.wpi.first.wpilibj.tables.ITable;



public class MockSpeedController implements SpeedController,
                                            LiveWindowSendable
{
    private static final Logger logger = LoggerFactory.getLogger(MockSpeedController.class);

    private double speed = 0;
    private boolean inverted = false;
    
    @Override
    public double get() { return speed; }


    @Override
    public void set(double speed)
    {
        this.speed = inverted ? -speed : speed;
    }


    @Override
    public void setInverted(boolean isInverted)
    {
        this.inverted = isInverted;
    }


    @Override
    public boolean getInverted()
    {
        return inverted;
    }


    @Override
    public void disable()
    {
        throw new UnsupportedOperationException("disable(0 not supported in this mock implementation.");
    }


    @Override
    public void stopMotor()
    {
        speed = 0;
    }


    @Override
    public void pidWrite(double output)
    {
        logger.info("pidWrite caleed with value of {}", output);
    }


    @Override
    public void updateTable()
    {
        

    }


    @Override
    public void startLiveWindowMode()
    {
        
    }


    @Override
    public void stopLiveWindowMode()
    {
        
    }


    @Override
    public void initTable(ITable subtable)
    {
        
    }


    @Override
    public ITable getTable()
    {
        return null;
    }


    @Override
    public String getSmartDashboardType()
    {
       return "mock";
    }
}
