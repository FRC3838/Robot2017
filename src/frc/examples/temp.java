package frc.examples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class temp
{
    private static final Logger logger = LoggerFactory.getLogger(temp.class);


    public static void main(String... args)
    {
        try
        {
            temp runner = new temp();
            runner.run();
        }
        catch (Exception ex)
        {
            logger.error("An exception occurred when running {}. Cause details: {}", temp.class.getSimpleName(), ex.toString(), ex);
        }
        System.exit(0);
    }


    private void run()
    {
        //  double povValue  = isX ? Math.sin(Math.toRadians(povDirection)) : Math.cos(Math.toRadians(povDirection));

        final double radians = Math.toRadians(180);
        logger.info("radians = '{}'", radians);
        final double cos = Math.cos(radians);
        logger.info("cos = '{}'", cos);
        final double sin = Math.sin(radians);
        logger.info("sin = '{}'", sin);



    }


    public enum DIR
    {
        N(0),
        NE(45),
        E(90),
        SE(135),
        S(180),
        SW(225),
        W(270),
        NW(315);

        private final int angle;
        private static final int THRESHOLD = 5;


        DIR(int angle)
        {
            this.angle = angle;
        }

//        boolean isActive(double readAngle)
//        {
//
//
//        }
    }

}
