package frc.team3838.game;

import java.util.Properties;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableMap;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.core.Abstract3838Robot;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.game.commands.autonomous.AutonomousSelector2;
import frc.team3838.game.subsystems.DriveTrainSubsystem;



/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
// *IMPORTANT: If you rename this class or change its package, the corresponding
//             change needs to be made in the ${projectRoot}/build.properties
//             file
public class Robot3838 extends Abstract3838Robot
{

    private static final String NAVX_ADJUSTMENT_ANGLE_PROP_KEY = "navxAdjustmentAngle";
    private static final double DEFAULT_NAVX_ADJUSTMENT_ANGLE = -0.825;
    private static final String DEFAULT_NAVX_ADJUSTMENT_ANGLE_STRING = Double.toString(DEFAULT_NAVX_ADJUSTMENT_ANGLE);

    private static final String ENCODER_DISTANCE_PER_PULSE_KEY = "encoderDistancePerPulse";
    private static final double DEFAULT_ENCODER_DISTANCE_PER_PULSE = 0.054917;
    private static final String DEFAULT_ENCODER_DISTANCE_PER_PULSE_STRING = Double.toString(DEFAULT_ENCODER_DISTANCE_PER_PULSE);


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Nullable
    @Override
    protected String getRobotInitVersionInfo()
    {
        return  "Code Version 34";
    }


    @Override
    protected Properties getDefaultProperties()
    {
        Properties properties = new Properties();
        properties.setProperty(getRobotNamePropertiesKey(), UNDETERMINED_ROBOT_NAME);
        properties.setProperty(NAVX_ADJUSTMENT_ANGLE_PROP_KEY, DEFAULT_NAVX_ADJUSTMENT_ANGLE_STRING);
        return properties;
    }


    @Override
    protected String getRobotPropertiesFileName()
    {
        return "2017.properties";
    }

    public static double getNavxAdjustmentAngle()
    {
        final Logger logger = LoggerFactory.getLogger(Robot3838.class);

        try
        {
            final String adjAngleString = getRobotProperties().getProperty(NAVX_ADJUSTMENT_ANGLE_PROP_KEY, DEFAULT_NAVX_ADJUSTMENT_ANGLE_STRING);
            final Double adjAngle = Double.valueOf(adjAngleString);
            logger.info("NavX Adjustment Angle of {} read in from properties file.", adjAngle);
            return adjAngle;
        }
        catch (Exception e)
        {
            logger.warn("Could not read NavX Adjustment Angle from robot properties. Using default angle of {}", DEFAULT_NAVX_ADJUSTMENT_ANGLE);
            return DEFAULT_NAVX_ADJUSTMENT_ANGLE;
        }
    }


    public static double getEncoderDistancePerPulse()
    {
        final Logger logger = LoggerFactory.getLogger(Robot3838.class);

        // Note 02/21/2017 - Bag & Tag night -  Robot 2 is dead on when running the drive straight 140"
        //                                      Robot 1 is off a little (~1 inch). So we still need to calibrate the encoder distance per pulse value a little

        try
        {
            final String valueString = getRobotProperties().getProperty(ENCODER_DISTANCE_PER_PULSE_KEY, DEFAULT_ENCODER_DISTANCE_PER_PULSE_STRING);
            final Double value = Double.valueOf(valueString);
            logger.info("Encoder Distance per Pulse setting of {} read in from properties file.", value);
            return value;
        }
        catch (Exception e)
        {
            logger.warn("Could not read Encoder Distance per Pulse setting from robot properties. Using default value of {}", DEFAULT_ENCODER_DISTANCE_PER_PULSE);
            return DEFAULT_ENCODER_DISTANCE_PER_PULSE;
        }
    }

    @Override
    protected void initAutonomousSelectorImpl() throws Exception
    {
        AutonomousSelector2.initializeChooser();
    }


    @Nullable
    @Override
    protected Command getSelectedAutonomousCommandImpl() throws Exception
    {
        return AutonomousSelector2.getSelectedCommand();
    }


    @Override
    protected void initJoysticks()
    {

    }


    @Override
    protected void additionalRobotInitWork()
    {

    }


    @Nonnull
    @Override
    protected ImmutableMap<Class<? extends I3838Subsystem>, Boolean> getEnabledSubsystemsMap() { return RobotMap.enabledSubsystemsMap; }


    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Nonnull
    @Override
    protected Abstract3838DriveTrainSubsystem getDriveTrainSubsystem()
    {
        return DriveTrainSubsystem.getInstance();
    }

}
