package frc.team3838.game.commands.camera.usb;

public class SwitchToRearUsbCameraCommand extends AbstractUsbCameraCommand
{
    /**
     * The execute method is called repeatedly when this Command is
     * scheduled to run until. It is called repeatedly until
     * the either isFinish returns true, interrupted is called,
     * or the command is canceled. Note that the initialize
     * method is called one time before execute is called the
     * first time. So do any setup work in the initialize
     * method. This method should run quickly. It should not
     * block for any period of time.
     */
    @Override
    protected void executeImpl() throws Exception
    {
        usbCameraSubsystem.switchToRearCamera();
    }
}
