package frc.team3838.game.commands.camera.usb;

import java.util.Set;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSet;

import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.game.subsystems.UsbCameraSubsystem;



public abstract class AbstractUsbCameraCommand extends Abstract3838Command
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractUsbCameraCommand.class);
    protected final UsbCameraSubsystem usbCameraSubsystem = UsbCameraSubsystem.getInstance();


    @Nonnull
    @Override
    protected Set<I3838Subsystem> getRequiredSubsystems()
    {
        return ImmutableSet.of(UsbCameraSubsystem.getInstance());
    }


    /**
     * The initialize method is called just before the first time
     * this Command is run after being started. For example, if
     * a button is pushed to trigger this command, this method is
     * called one time after the button is pushed. Then the execute
     * command is called repeated until isFinished returns true,
     * or interrupted is called by the command scheduler/runner.
     * After isFinished returns true, end is called one time
     * in order to do any cleanup or set and values.
     */
    @Override
    protected void initializeImpl() throws Exception
    {

    }


    /**
     * <p>
     * Returns whether this command is finished. If it is, then the command will be removed and
     * {@link #end()} will be called.
     * </p><p>
     * It may be useful for a team to reference the {@link #isTimedOut()}
     * method for time-sensitive commands.
     * </p><p>
     * Returning false will result in the command never ending automatically. It may still be
     * cancelled manually or interrupted by another command. Returning true will result in the
     * command executing once and finishing immediately. It is recommended to use
     * {@link edu.wpi.first.wpilibj.command.InstantCommand} (added in 2017) for this.
     * </p>
     *
     * @return whether this command is finished.
     *
     * @see #isTimedOut() isTimedOut()
     */
    @Override
    protected boolean isFinishedImpl() throws Exception
    {
        return true;
    }


    /**
     * Called once when the command ended peacefully; that is it is called once
     * after {@link #isFinished()} returns true. This is where you may want to
     * wrap up loose ends, like shutting off a motor that was being used in the
     * command.
     */
    protected void endImpl() throws Exception
    {

    }


    /**
     * <p>
     * Called when the command ends because somebody called {@link #cancel()} or
     * another command shared the same requirements as this one, and booted it out. For example,
     * it is called when another command which requires one or more of the same
     * subsystems is scheduled to run.
     * </p><p>
     * This is where you may want to wrap up loose ends, like shutting off a motor that was being
     * used in the command.
     * </p><p>
     * Generally, it is useful to simply call the {@link #end()} method within this
     * method, as done here.
     * </p>
     */
    protected void interruptedImpl() throws Exception
    {
        super.interruptedImpl();
    }
}
