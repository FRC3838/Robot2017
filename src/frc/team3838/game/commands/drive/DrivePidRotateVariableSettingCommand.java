package frc.team3838.game.commands.drive;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;



public class DrivePidRotateVariableSettingCommand extends DrivePidRotateCommand
{
    private static final Logger logger = LoggerFactory.getLogger(DrivePidRotateVariableSettingCommand.class);

    private static AtomicInteger instanceNum = new AtomicInteger(0);

    private final String sdbName;


    public DrivePidRotateVariableSettingCommand()
    {
        super(DrivePidRotateVariableSettingCommand.class.getSimpleName() + "_#" + instanceNum.incrementAndGet());

        sdbName = "Variable Rotate Cmd #" + instanceNum.get() + " Target Angle";

        boolean isSet = false;

        final double startingValue = SmartDashboard.getNumber(sdbName, 0);
        SmartDashboard.putNumber(sdbName, startingValue);

    }


    @Override
    protected void initializeImpl() throws Exception
    {
        targetAngle = SmartDashboard.getNumber(sdbName, 0);
        super.initializeImpl();
    }
}
