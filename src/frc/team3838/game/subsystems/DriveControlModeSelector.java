package frc.team3838.game.subsystems;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.meta.API;

import static frc.team3838.game.subsystems.DriveControlModeSelector.RobustDriveControlMode.ArcadeViaGamepad;



public class DriveControlModeSelector
{
    private static final Logger logger = LoggerFactory.getLogger(DriveControlModeSelector.class);

    public static final SendableChooser<RobustDriveControlMode> chooser = new SendableChooser<>();

    final Path persistFile = Paths.get("/home/lvuser/Team3838/DriveControlMode.txt");

    final RobustDriveControlMode initialDefaultMode = ArcadeViaGamepad;


    public enum RobustDriveControlMode
    {
        @API ArcadeViaGamepad("Arcade Control using a Gamepad"),
        @API ArcadeViaJoystick("Arcade Control using a Joystick"),
        @API TankViaGamepad("Tank Control using a Gamepad"),
        @API TankViaJoystick("Tank Control using two Joysticks");

        private final String description;


        RobustDriveControlMode(String description)
        {
            this.description = description;
        }


        public String getDescription()
        {
            return description;
        }
    }


    public DriveControlModeSelector()
    {
        final RobustDriveControlMode defaultMode = readValue();

        final RobustDriveControlMode[] modes = RobustDriveControlMode.values();

        for (RobustDriveControlMode mode : modes)
        {
            if (mode == defaultMode)
            {
                chooser.addDefault(mode.getDescription(), mode);
            }
            else
            {
                chooser.addObject(mode.getDescription(), mode);
            }
        }

        SmartDashboard.putData("Drive Control Modes Selection...", chooser);
    }

    public RobustDriveControlMode getMode()
    {
        return chooser.getSelected();
    }

    public void persistSelectedMode()
    {
        writeValue(chooser.getSelected());
    }

    protected RobustDriveControlMode readValue()
    {
        if (!Files.exists(persistFile))
        {
            logger.info("DriveControlModeSelector file does not exist. Using initialDefaultMode of {}", initialDefaultMode);
            return initialDefaultMode;
        }

        if (!Files.isReadable(persistFile))
        {
            logger.info("DriveControlModeSelector file is not readable. Using initialDefaultMode of {}", initialDefaultMode);
            return initialDefaultMode;
        }

        try(BufferedReader reader = Files.newBufferedReader(persistFile, StandardCharsets.UTF_8))
        {
            final String line = reader.readLine();
            return RobustDriveControlMode.valueOf(line);
        }
        catch (Exception e)
        {
            logger.info("An Exception occurred when reading DriveControlModeSelector file. Using initialDefaultMode of {}. Cause Summary: {}", initialDefaultMode, e.toString(), e);
            return initialDefaultMode;
        }
    }


    protected void writeValue(RobustDriveControlMode mode)
    {
        try
        {
            Files.createDirectories(persistFile);
            try ( final PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(persistFile.toFile(), false), StandardCharsets.UTF_8), true))
            {
                writer.println(mode.name());
                writer.flush();
            }
        }
        catch (Exception e)
        {
            logger.warn("Could not write to DriveControlModeSelector file due to an Exception. Cause summary: {}", e.toString(), e);
        }
    }
}
