package frc.team3838.game.subsystems;

import java.util.Set;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSet;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.buttons.Trigger;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.controls.InvertibleDigitalInput;
import frc.team3838.core.controls.Throttle;
import frc.team3838.core.hardware.MotorOps;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.Abstract3838Subsystem;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.game.RobotMap;
import frc.team3838.game.RobotMap.PWMs;
import frc.team3838.game.RobotMap.UI;
import frc.team3838.game.commands.drive.DriverControlConfig2017;



/**
 * Runs the climber. <strong>An important note on operation: </strong>
 * A ratchet is used to prevent back spin on the climber. If the motor is turns
 * in the opposite direction of the ratchet setting, a gear on the climb drive will
 * break. It is therefore imperative that the motor turn in the proper direction.
 * During competition, the ratchet is of course set to climb. The throttle
 * will only allow the motor to turn in climb direction. In the pits when we go to
 * unwind the rope, the ratchet is put in reverse. For the motor to turn in reverse
 * a safety switch on the robot and and a trigger on the HID both have to be engaged.
 * If one is engaged and not the other, the motor will not turn. If both are engaged,
 * the motor will only turn in reverse/unwind. If neither are engaged, the motor will
 * only turn in forward/climb/wind.
 */
public class ClimberSubsystem extends Abstract3838Subsystem
{

    private static final Logger logger = LoggerFactory.getLogger(ClimberSubsystem.class);

    public static final double MIN_CLIMB_ABS_SPEED = 0.19;
    public static final double MAX_CLIMB_ABS_SPEED = 1.0;
    public static final double AUTO_START_CLIMB_ABS_SPEED = MIN_CLIMB_ABS_SPEED;

    private static final double AUTO_ON_START_REMAINING_TIME = 30.0;

    private MotorOps motorOps;

    private boolean hasAutoStartOccurred = false;
    private boolean hasManualControlOccurred = false;

    private static final double JOYSTICK_DRIFT_ZONE_THRESHOLD = 0.005;

    private static final JoystickMovement JOYSTICK_DIRECTION_TO_CLIMB = JoystickMovement.PUSH;



    private final DigitalInput reverseSafetyPhysicalSwitch = new InvertibleDigitalInput(RobotMap.DIOs.CLIMB_REVERSE_PHYSICAL_SAFETY_SWITCH, true);

    public static final String SDB_SAFETY_SWITCH_NAME = " Climb motor reverse safety switch ";

    @SuppressWarnings("unused")
    private enum JoystickMovement {PUSH, PULL}

    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     */
    private ClimberSubsystem()
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem();
        super();
        // ** DO NOT PUT ANY CODE IN THIS CONSTRUCTOR **
        // ** Do all initialization work in the initSubsystem() method
    }


    @Override
    protected void initSubsystem() throws Exception
    {
        // The initSubsystem() method is called by the super class constructor if,
        // and only if, the subsystem is enabled The super constructor will safely
        // handle this init method throwing an exception by disabling the subsystem
        final Spark spark = new Spark(PWMs.CLIMBER_MOTOR);
        // By default, a negative speed is climb. So we invert that for better intuitiveness
        motorOps = new MotorOps(spark, true);
        LiveWindow.addSensor(getName(), "Physical Safety Switch [" + RobotMap.DIOs.CLIMB_REVERSE_PHYSICAL_SAFETY_SWITCH + ']', reverseSafetyPhysicalSwitch);
    }


    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        // Initialize the default command, if any, here.
        // A default command runs without any driver action
        // Default Commands are not common for most subsystems
        setDefaultCommand(new Abstract3838Command() {
            @Nonnull
            @Override
            protected Set<I3838Subsystem> getRequiredSubsystems()
            {
                return ImmutableSet.of(ClimberSubsystem.getInstance());
            }


            @Override
            protected void initializeImpl() throws Exception { updateSafetySwitchStatus(); }


            @Override
            protected void executeImpl() throws Exception { operate(); }


            @Override
            protected boolean isFinishedImpl() throws Exception { return false; }


            @Override
            protected void endImpl() throws Exception { }
        });
    }


    private void updateSafetySwitchStatus()
    {
        SmartDashboard.putBoolean(SDB_SAFETY_SWITCH_NAME, reverseSafetyPhysicalSwitch.get());
    }


    public void operate()
    {
        updateSafetySwitchStatus();

        // States
        //      1) Autonomous
        //          - reset hasAutoStartOccurred flag
        //          - no operation of motor
        //      2) Teleop
        //          2a) Non Comp or Practice
        //              - Just let the motor run
        //          2b) Comp or Practice
        //              2b1) If >= 30 seconds
        //                   - no motor control
        //              2b2) If < 30 seconds
        //                  2b2a) If auto mode has not started
        //                      - set the autoModeHasStarted flag to true
        //                      - start the motor



        double speed;
        final DriverControlConfig2017 configuration = UI.getDriveTrainControlConfiguration();
        final Throttle throttle = configuration.getClimbMotorThrottle();

        if (isAutonomous())
        {
            // CASE 1: reset the flag
            hasAutoStartOccurred = false;
           speed = 0;
        }
        else
        {
            // CASE 2: We are in Teleop (or Test)

            if (isNotInCompOrPracticeMode())
            {
                // CASE 2a - we are not in comp or practice, so just revert to manual throttle control
                speed = getThrottleSpeed(throttle);
            }
            else
            {
                if (getMatchTime() > AUTO_ON_START_REMAINING_TIME)
                {
                    // CASE 2b1 - In comp/practice, teleop, but prior to the end game - no motor control
                    speed = 0;
                }
                else
                {
                    // CASE 2b2 - In comp/practice, teleop, in end game time frame
                    if (!hasAutoStartOccurred)
                    {
                        // CASE 2b2a -
                        // We have not yet auto started the motor, so lets do that, and set the flag
                        hasAutoStartOccurred = true;
                        speed = AUTO_START_CLIMB_ABS_SPEED;
                        logger.info("Auto starting climber");
                    }
                    else // auto start HAS occurred
                    {
                        // auto start has occurred.
                        if (hasManualControlOccurred)
                        {
                            // Manual control has occurred, so we defer to the throttle
                            speed = getThrottleSpeed(throttle);
                        }
                        else
                        {
                            // manual control has not occurred - we need to determine if the throttle has been activated yet
                            if (Math.abs(throttle.get()) > JOYSTICK_DRIFT_ZONE_THRESHOLD)
                            {
                                // The throttle has been activated, so we set the flag and use that speed
                                logger.info("Stopping auto running of climber. Reverting to manual control.");
                                hasManualControlOccurred = true;
                                speed = getThrottleSpeed(throttle);
                            }
                            else
                            {
                                // Auto start has occurred, but the driver has not yet
                                // done any manual control, so we keep the auto mode speed
                                speed = AUTO_START_CLIMB_ABS_SPEED;
                            }
                        }
                    }
                }
            }
        }

        final Trigger reverseSafetyUiTrigger = configuration.getAllowReverseClimbTrigger();


        // Three modes
        //    1) if both physical Switch and joystick switch are NOT engaged - allow forward only
        //    2) if both physical Switch and joystick switch are engaged - allow reverse only
        //    3) if one but not the other switches are engaged - do nothing

        // forward is negative
        speed =
            (reverseSafetyUiTrigger.get() && reverseSafetyPhysicalSwitch.get() && speed < 0) ||
            (!reverseSafetyUiTrigger.get() && !reverseSafetyPhysicalSwitch.get() && speed > 0)
            ? speed
            : 0;

        motorOps.setSpeed(speed);

    }


    private static double getThrottleSpeed(Throttle throttle)
    {
        double speed =  throttle.get();

        // If we want PUSH to Climb, we need to invert the Joystick value since pushing is a negative number

        if (JOYSTICK_DIRECTION_TO_CLIMB == JoystickMovement.PUSH)
        {
            speed = -speed;
        }

        //compensate for dead zone on Joystick
        if (Math.abs(speed) < JOYSTICK_DRIFT_ZONE_THRESHOLD)
        {
            speed = 0;
        }
        return speed;
    }




    private static boolean isAutonomous() { return DriverStation.getInstance().isAutonomous(); }


    private static double getMatchTime() { return DriverStation.getInstance().getMatchTime(); }

    @API
    private static boolean isTeleop() { return DriverStation.getInstance().isOperatorControl(); }


    @API
    private static boolean isTeleopInCompOrPractice() { return isInCompOrPracticeMode() && DriverStation.getInstance().isOperatorControl(); }




    @API
    private static boolean isInCompOrPracticeMode() { return getMatchTime() != -1; }


    @API
    private static boolean isNotInCompOrPracticeMode() { return getMatchTime() == -1; }

    public void stop() { motorOps.stop(); }













    /* ********************************************************************************************* */
    /* NO NON-SINGLETON INITIALIZATION CODE BELOW THIS POINT                                         */
    /* There is the possibility for a very subtle bug to occur if any other field declarations       */
    /* are placed after the singleton field declaration.                                             */
    /* ********************************************************************************************* */


    /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final ClimberSubsystem singleton = new ClimberSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static ClimberSubsystem getInstance() {return singleton;}


    /* ********************************************************************************************* */
    /*  NO CODE BELOW THIS POINT !!                                                                  */
    /* ********************************************************************************************* */


}

