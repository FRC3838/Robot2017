package frc.team3838.core.hardware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ctre.CANTalon;
import com.ctre.CANTalon.TalonControlMode;

import frc.team3838.core.meta.API;


@API
public final class SpeedControllerUtils
{
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(SpeedControllerUtils.class);


    private SpeedControllerUtils() { }


    @API
    public static CANTalon createCanTalon(int channel)
    {
        return createCanTalon(channel, TalonControlMode.PercentVbus);
    }

    @API
    public static CANTalon createCanTalon(int channel, TalonControlMode talonControlMode)
    {
        CANTalon canTalon = new CANTalon(channel);
        canTalon.enableBrakeMode(true);
        canTalon.changeControlMode(talonControlMode);
        canTalon.enableControl();
        return canTalon;
    }
}
