package frc.team3838.core.hardware;

import edu.wpi.first.wpilibj.SpeedController;



public class NoOpSpeedController implements SpeedController
{
    @Override
    public double get() { return 0; }


    @Override
    public void set(double speed) { }


    @Override
    public void setInverted(boolean isInverted) { }


    @Override
    public boolean getInverted() { return false; }


    @Override
    public void disable() { }


    @Override
    public void stopMotor() { }


    @Override
    public void pidWrite(double output) { }
}
