package frc.team3838.core.hardware;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDInterface;
import edu.wpi.first.wpilibj.Sendable;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.livewindow.LiveWindowSendable;
import edu.wpi.first.wpilibj.tables.ITable;
import edu.wpi.first.wpilibj.tables.ITableListener;
import frc.team3838.core.meta.API;
import frc.team3838.core.utils.MathUtils;



/**
 * A SpeedController to wrap another speed controller that scales the set value to a 'usable range'.
 * For example, say there is a motor that stalls at ±0.25 or lower, and we do not want to go faster than
 * ±0.85. It's (absolute) usable range then is 0.25 to 0.85, a range of 0.6. If we get Joystick input of 0.5 
 * (to indicate 50%), we do not want to set the motor to 0.5, but rather half of its usable range. 
 * Half of the 0.6 range is 0.3, added to the min speed of 0.253 tells us that 50% of the usable range is
 * 0.55 (0.25 + 0.3). Thus, when configured with a min speed of 0.25 and a max speed of 0.85,  a call to 
 * this class' <tt>set(0.5)</tt> will call the wrapped SpeedController with <tt>set(0.55)</tt>. Similarly,
 * a value of -0.5 (i.e. 50% in reverse) would set -0.55 on the wrapped controller.
 * <p>
 * <strong>Important:</strong> This class can only wrap speed controllers that have a speed range of -1.0 to 1.0.
 * It cannot wrap PWM based controllers that use a range of 0 to 255.
 */
@API
public class ScalingSpeedController implements SpeedController,
                                               LiveWindowSendable
{
    private static final Logger logger = LoggerFactory.getLogger(ScalingSpeedController.class);

    @Nonnull
    private final SpeedController wrappedSpeedController;
    
    @Nullable
    private final LiveWindowSendable liveWindowSendable;
    
    @Nullable
    private final Sendable sendable;

    private final double minOrStallSpeed;
    private final double maxSpeed;

    
    @API
    public ScalingSpeedController(@Nonnull SpeedController wrappedSpeedController) 
    {
        this(wrappedSpeedController, 0, 1.0);
    }


    @API
    public ScalingSpeedController(@Nonnull SpeedController wrappedSpeedController, double minOrStallSpeed) 
    {
        this(wrappedSpeedController, minOrStallSpeed, 1.0);
    }


    @API
    public ScalingSpeedController(@Nonnull SpeedController wrappedSpeedController, double minOrStallSpeed, double maxSpeed) 
    {
        this.wrappedSpeedController = wrappedSpeedController;
        this.minOrStallSpeed = minOrStallSpeed;
        this.maxSpeed = maxSpeed;
        this.liveWindowSendable = (wrappedSpeedController instanceof LiveWindowSendable) ? (LiveWindowSendable) wrappedSpeedController : null;
        this.sendable = (wrappedSpeedController instanceof Sendable) ? (Sendable) wrappedSpeedController : null;
    }


    @Override
    public double get() {return wrappedSpeedController.get();}


    @Override
    public void set(double speed) 
    {
        double scaledSpeed = MathUtils.scaleRange(Math.abs(speed), 0, 1.0, minOrStallSpeed, maxSpeed);
        scaledSpeed = MathUtils.isNegative(speed) ? -scaledSpeed : scaledSpeed;
        logger.trace("Scaling speed of {} to {} (within the range of {} to {})", speed, scaledSpeed, minOrStallSpeed, maxSpeed);
        wrappedSpeedController.set(scaledSpeed);
    }


    @Override
    public void setInverted(boolean isInverted) {wrappedSpeedController.setInverted(isInverted);}


    @Override
    public boolean getInverted() {return wrappedSpeedController.getInverted();}


    @Override
    public void disable() {wrappedSpeedController.disable();}


    @Override
    public void stopMotor() {wrappedSpeedController.stopMotor();}


    @Override
    public void pidWrite(double output) {wrappedSpeedController.pidWrite(output);}


    @Nullable
    private ITable table = null;
    @Nullable
    private ITableListener tableListener = null;
    public static final String SMART_DASHBOARD_TYPE = "ScalingSpeedController";
    
    @Override
    public void updateTable()
    {
        if (liveWindowSendable != null)
        {
            liveWindowSendable.updateTable();
        }
        else
        {
            if (table != null)
            {
                table.putString("~TYPE~", SMART_DASHBOARD_TYPE);
                table.putString("Type", getClass().getSimpleName());
                
                if (wrappedSpeedController instanceof PIDInterface)
                {
                    final PIDInterface pid = (PIDInterface) this.wrappedSpeedController;
                    pid.getP();
                    table.putNumber("p", pid.getP());
                    table.putNumber("i", pid.getI());
                    table.putNumber("d", pid.getD());
                    if (wrappedSpeedController instanceof PIDController)
                    {
                        table.putNumber("f", ((PIDController) wrappedSpeedController).getF());
                    }
                    table.putBoolean("Enabled", pid.isEnabled());
                }
                table.putNumber("Value", get());
            }
        }
    }


    @Override
    public void startLiveWindowMode()
    {
        if (liveWindowSendable != null)
        {
            liveWindowSendable.startLiveWindowMode();
        }
        else 
        {
            if (table != null)
            {
                set(0); // Stop for safety
                tableListener = createTableListener();
                table.addTableListener(tableListener, true);
            }
        }
    }
    
    private ITableListener createTableListener()
    {
        return (table, key, value, isNew) ->
        {
            switch (key)
            {
                case "Enabled":
                    if ((Boolean) value)
                    {
                        if (wrappedSpeedController instanceof PIDInterface)
                        {
                            ((PIDInterface) wrappedSpeedController).enable();
                        }
                    }
                    else
                    {
                        disable();
                    }
                    break;
                case "Value":
                    set((Double) value);
                    break;
                default:
                    break;
            }
            if (wrappedSpeedController instanceof PIDController)
            {
                final PIDController pid = (PIDController) this.wrappedSpeedController;
                switch (key)
                {
                    case "p":
                        //noinspection ConstantConditions
                        pid.setPID((Double) value, pid.getI(), pid.getD(), pid.getF());
                        break;
                    case "i":
                        //noinspection ConstantConditions
                        pid.setPID(pid.getP(), (Double) value, pid.getD(), pid.getF());
                        break;
                    case "d":
                        //noinspection ConstantConditions
                        pid.setPID(pid.getP(), pid.getI(), (Double) value, pid.getF());
                        break;
                    case "f":
                        //noinspection ConstantConditions
                        pid.setPID(pid.getP(), pid.getI(), pid.getD(), (Double) value);
                        break;
                    default:
                        break;
                }
            }
        };
    }
    


    @Override
    public void stopLiveWindowMode()
    {
        if (liveWindowSendable != null)
        {
            liveWindowSendable.stopLiveWindowMode();
        }
        else
        {
            if (table != null)
            {
                set(0); // Stop for safety
                table.removeTableListener(tableListener);
            }
        }
    }


    @Override
    public void initTable(ITable subtable)
    {
        if (sendable != null)
        {
            sendable.initTable(subtable);
        }
        else
        {
            table = subtable;
            updateTable();
        }
    }


    @Override
    public ITable getTable()
    {
        if (sendable != null)
        {
            return sendable.getTable();
        }
        else
        {
            return table;
        }
    }


    @Override
    public String getSmartDashboardType()
    {
        if (sendable != null)
        {
            return sendable.getSmartDashboardType();
        }
        else
        {
            return SMART_DASHBOARD_TYPE;
        }
    }
}
