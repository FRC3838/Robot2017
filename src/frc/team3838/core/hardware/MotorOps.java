package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.annotations.VisibleForTesting;

import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.SpeedController;
import frc.team3838.core.meta.Max;
import frc.team3838.core.meta.Min;
import frc.team3838.core.utils.MathUtils;



@SuppressWarnings({"unused", "WeakerAccess"})
public class MotorOps implements PIDOutput
{
    private static final Logger logger = LoggerFactory.getLogger(MotorOps.class);

    // Motor settings
    //    setRaw() ranges from 0 to 255. 128 to 255 is forward speed, and 127 to 0 is reversed
    //    set() ranges from -1 to 1 and is provided for victor.

    private static final double STOP = 0;

    public static final double DEFAULT_MIN_SPEED = 0.01;

    private final boolean invert;
    private final int invertFactor;

    private final double minOrStallSpeed;
    private final double maxSpeed;

    private SpeedController controller;



    public enum Direction { Forward, Stopped, Reverse }

    /**
     * @param speedController the {@link SpeedController} (CANJaguar, Jaguar, Talon or Victor) to control
     * @param invert          if the motor should be inverted via software
     */
    public MotorOps(SpeedController speedController, boolean invert)
    {
        this(speedController, invert, DEFAULT_MIN_SPEED);
    }


    /**
     * @param speedController the {@link SpeedController} (CANJaguar, Jaguar, Talon or Victor) to control
     * @param invert          if the motor should be inverted via software
     * @param minOrStallSpeed the minimum speed (to prevent motor stalling or burn out); must be between
     *                        0.005 (i.e. ½ percent) and 1.0
     */
    public MotorOps(SpeedController speedController, boolean invert, double minOrStallSpeed)
    {
        this(speedController, invert, minOrStallSpeed, 1.0);
    }

    public MotorOps(SpeedController speedController, boolean invert, @Min(0) @Max(0.99) double minOrStallSpeed, @Min(0.01) @Max(1.0) double maxSpeed)
    {

        if ((minOrStallSpeed < 0) || (minOrStallSpeed > 0.99))
        {
            throw new IllegalArgumentException("The minimum/stall speed setting must be between 0 and 0.99. It was " + minOrStallSpeed);
        }
        if ((maxSpeed < 0.01) || (minOrStallSpeed > 1.0))
        {
            throw new IllegalArgumentException("The maximum speed setting must be between 0.01 and 1.0 It was " + maxSpeed);
        }
        if (maxSpeed <= minOrStallSpeed)
        {
            throw new IllegalArgumentException("The maximum speed setting must be greater than the minimum/stall speed. Min speed was " + minOrStallSpeed + ", and max speed was " + maxSpeed);
        }


        this.controller = speedController;
        this.invert = invert;
        invertFactor = invert ? -1 : 1;



        this.minOrStallSpeed = minOrStallSpeed;
        this.maxSpeed = maxSpeed;
        speedController.set(STOP);
    }


    public SpeedController getSpeedController()
    {
        return controller;
    }





    public boolean isInverted()
    {
        return invert;
    }


    /**
     * Gets the speed, corrected for an inverted motor, set on the speed
     * controller as a value between -1.0 and 1.0.
     *
     * @return The speed set on the speed controller as a value between -1.0 and 1.0.
     */
    @Min(-1.0)
    @Max(1.0)
    public double getSpeed()
    {
        return controller.get() * invertFactor;
    }

    /**
     * Sets the speed as a <strong>value between -1.0 and 1.0 (inclusive)</strong> such that
     * a positive speed value is forward, a negative speed is reverse, and zero is stopped.
     * Any values outside the inclusive range will be constrained (<em>not</em> scaled) to
     * between -1 and  1.
     *
     * @param speed the speed (between -1 and 1 inclusive) such that a positive speed is forward,
     *              a negative speed is reverse, and zero is stopped. Any values outside the inclusive
     *              range will be constrained (<em>not</em> scaled).
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    @SuppressWarnings("UnusedReturnValue")
    public String setSpeed(@Min(-1.0) @Max(1.0) double speed)
    {
        final double absoluteSpeed = Math.abs(speed);
        if (absoluteSpeed < minOrStallSpeed)
        {
            speed = 0;
        }

        if (absoluteSpeed > maxSpeed)
        {
            speed = MathUtils.isNegative(speed) ? -maxSpeed : maxSpeed;
        }
        final double newSpeed = speed * invertFactor;
        controller.set(newSpeed);
        return getSpeedPercentage();
    }


    /**
     * Sets the speed in a specified direction.
     *
     * @param absoluteSpeed     the absolute speed value, between 0 and 1.0, to set. Values less than 0 will
     *                  be constrained to zero. Values greater than 1.0 will be constrained to 1.0.
     * @param direction the direction to put the motor in. If set to {@link Direction#Stopped}
     *                  the speed value will be ignored.
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String setSpeed(@Min(0) @Max(1.0) double absoluteSpeed, @Nonnull Direction direction)
    {
        absoluteSpeed = MathUtils.constrainValue(absoluteSpeed, minOrStallSpeed, maxSpeed);
        switch (direction)
        {
            case Stopped:
                setSpeed(STOP);
                break;
            case Forward:
                setSpeed(absoluteSpeed);
                break;
            case Reverse:
                setSpeed(-absoluteSpeed);
                break;
        }
        return getSpeedPercentage();

    }

    /**
     * Sets the forward speed as a percentage between 0 and 100 (inclusive) such that
     * 0 is stop, 100 is full speed in forward, 50 is 1/2 speed in forward. etc.
     *
     * @param percentage the forward speed as a percentage between 0 and 100 (inclusive)
     */
    public void setForwardSpeedAsPercent(@Min(0) @Max(100) int percentage)
    {
        double targetSpeed = MathUtils.constrainValue(percentage, 0, 100) / 100;
        setSpeed(targetSpeed);
    }


    /**
     * Sets the reverse speed as a percentage between 0 and 100 (inclusive) such that
     * 0 is stop, 100 is full speed in reverse, 50 is 1/2 speed in reverse. etc.
     *
     * @param percentage the reverse speed as a percentage between 0 and 100 (inclusive)
     */
    public void setReverseSpeedAsPercent(@Min(0) @Max(100) int percentage)
    {
        double targetSpeed = MathUtils.constrainValue(percentage, 0, 100) / 100;
        setSpeed(-targetSpeed);
    }


    /**
     * Modifies the speed by the supplied delta (a value between -1 .0 and 1.0 ) without allowing the
     * motor to reverse direction. A positive value means to increase the speed in the current
     * direction. A negative value means to decrease the motor speed in the current direction.
     * Some Examples:
     * <table>
     * <tr><th>Current Speed</th><th>delta</th><th>New Speed</th></tr>
     * <tr><td>0.5 (i.e. Forward 50%)</td><td>-0.7 (decrease 0.7)</td><td>0.0 (i.e. stopped)</td></tr>
     * <tr><td>0.8 (i.e. Forward 80%)</td><td>-0.6 (decrease 0.6)</td><td>0.2 (i.e. Forward 20%)</td></tr>
     * <tr><td>0.3 (i.e. Forward 30%)</td><td>0.8 (increase 0.8)</td><td>1.0 (i.e. Forward 100%)</td></tr>
     * </table>
     *
     * @param delta the amount to change the speed by between -1.0 and 1.0 inclusive such that a
     *              negative number will decrease the speed and a positive number will increase it
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    @VisibleForTesting
    protected final String modifySpeedWithOutDirectionChange(@Min(-1.0) @Max(1.0) double delta)
    {
        final Direction currentDirection = getDirection();

        if (currentDirection == Direction.Stopped)
        {
            return getSpeedPercentage();
        }
        else
        {
            double newSpeed = getAbsoluteSpeed() + delta;
            if (currentDirection == Direction.Forward)
            {
                newSpeed = MathUtils.constrainBetweenZeroAndOne(newSpeed);
            }
            else
            {
                newSpeed = MathUtils.constrainBetweenNegOneAndZero(-newSpeed);
            }
            return setSpeed(newSpeed);
        }
    }


    /**
     * Toggles the motor to run at the current speed, but in the opposite direction.
     * Calling on a stopped motor would have no effect.
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String toggleDirection()
    {
        setSpeed(-getSpeed());
        return getSpeedPercentage();
    }


    /**
     * Stops the motor.
     *
     * @return The speed percentage as a formatted string as described in {@link #getSpeedPercentage()}.
     * In this case, the value 'STOPPED' is returned.
     */
    public String stop()
    {
        controller.set(STOP);
        return getSpeedPercentage();
    }


    /**
     * Increases the speed in the currently traveling direction. If the motor is stopped, no action is taken.
     *
     * @param delta the absolute value, between 0.0 and 1.0, to increase the speed by
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String increaseSpeed(@Min(0.0) @Max(1.0) double delta)
    {
        final Direction currentDirection = getDirection();


        switch (currentDirection)
        {
            case Stopped:
                return getSpeedPercentage();
            case Forward:
                return increaseForwardSpeed(delta);
            case Reverse:
                return increaseReverseSpeed(delta);
            default:
                logger.error("Invalid Direction Case reached in increaseSpeed(double)");
                return getSpeedPercentage();
        }
    }


    /**
     * Decreases the speed in the currently traveling direction without reversing the direction of the
     * motor. Thus if the delta is greater than the(absolute value of) the current speed, the motor is
     * stopped. If the motor is stopped, no action is taken.
     *
     * @param delta the absolute value, between 0.0 and 1.0, to decrease the speed by
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String decreaseSpeed(@Min(0.0) @Max(1.0) double delta)
    {
        switch (getDirection())
        {
            case Stopped:
                return getSpeedPercentage();
            case Forward:
                return decreaseForwardSpeed(delta);
            case Reverse:
                return decreaseReverseSpeed(delta);
            default:
                logger.error("Invalid Direction Case reached in increaseSpeed(double)");
                return getSpeedPercentage();
        }
    }


    /**
     * Decreases the forward speed by the specified amount, but does not allow the motor to go into reverse.
     * For example, if the current speed is 5% (i.e. 0.05) forward and a delta value of 8% (i.e. 0.08) is
     * passed in, the motor speed will be set to 0, not 3% reversed. If the motor is stopped or running
     * in reverse, no action is taken.
     *
     * @param delta the amount to decrease as a value between 0 and 1.
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String decreaseForwardSpeed(@Min(0.0) @Max(1.0) double delta)
    {
        switch (getDirection())
        {
            case Forward:
                return modifySpeedWithOutDirectionChange(-Math.abs(delta));
            case Stopped:
            case Reverse:
            default:
                return getSpeedPercentage();
        }
    }


    /**
     * Decreases the forward speed by the specified percentage, between 0 and 100 (inclusive), but does
     * not allow the motor to go into reverse. For example, if the current speed is 5% (i.e. 0.05) forward
     * and a delta value of 8% (i.e. 0.08) is passed in, the motor speed will be set to 0, not 3% reversed.
     *
     * @param percentDelta the percentage to decrease the speed by
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String decreaseForwardSpeedByPercentage(@Min(0) @Max(100) int percentDelta)
    {
        return decreaseForwardSpeed((MathUtils.constrainValue(Math.abs(percentDelta), 0, 100)) / 100);
    }


    /**
     * Increases the forward speed by the specified amount. If the motor is stopped or running
     * in reverse, no action is taken.
     *
     * @param delta the amount to decrease as a value between 0 and 1.
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String increaseForwardSpeed(@Min(0.0) @Max(1.0) double delta)
    {
        switch (getDirection())
        {
            case Forward:
                return modifySpeedWithOutDirectionChange(Math.abs(delta));
            case Stopped:
            case Reverse:
            default:
                return getSpeedPercentage();
        }
    }


    /**
     * Increases the forward speed by the specified percentage, between 0 and 100 (inclusive).
     *
     * @param percentDelta the percentage to increase the speed by
     *
     * @return the speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String increaseForwardSpeedByPercentage(@Min(0) @Max(100) int percentDelta)
    {
        return increaseForwardSpeed((MathUtils.constrainValue(Math.abs(percentDelta), 0, 100)) / 100);
    }


    /**
     * Increases the reverse speed by the specified amount. If the motor is stopped or running
     * forward, no action is taken.
     *
     * @param delta the absolute amount to increase as a value between 0 and 1.
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String increaseReverseSpeed(double delta)
    {
        switch (getDirection())
        {
            case Reverse:
                return modifySpeedWithOutDirectionChange(Math.abs(delta));
            case Stopped:
            case Forward:
            default:
                return getSpeedPercentage();
        }
    }


    /**
     * Increases the reverse speed by the specified percentage, between 0 and 100 (inclusive).
     *
     * @param percentDelta the percentage to increase the speed by
     *
     * @return the speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String increaseReverseSpeedByPercentage(@Min(0) @Max(100) int percentDelta)
    {
        return increaseReverseSpeed((MathUtils.constrainValue(Math.abs(percentDelta), 0, 100)) / 100);
    }


    /**
     * Decreases the reverse speed by the specified amount, , but does not allow the motor to go into reverse.
     * If the motor is stopped or running forward, no action is taken.
     *
     * @param delta the absolute amount to increase as a value between 0 and 1.
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String decreaseReverseSpeed(double delta)
    {
        switch (getDirection())
        {
            case Reverse:
                return modifySpeedWithOutDirectionChange(-Math.abs(delta));
            case Stopped:
            case Forward:
            default:
                return getSpeedPercentage();
        }
    }


    /**
     * Decreases the reverse speed by the specified percentage, between 0 and 100 (inclusive), but does
     * not allow the motor to go into forward. For example, if the current speed is 5%  reverse (i.e. -0.05)
     * and a delta value of 8% (i.e. 0.08) is passed in, the motor speed will be set to 0, not 3% forward.
     *
     * @param percentDelta the percentage to decrease the speed by
     *
     * @return the new speed percentage as a formatted string as described in {@link #getSpeedPercentage()}
     */
    public String decreaseReverseSpeedByPercentage(@Min(0) @Max(100) int percentDelta)
    {
        return decreaseReverseSpeed((MathUtils.constrainValue(Math.abs(percentDelta), 0, 100)) / 100);
    }


    public boolean isRunning()
    {
        return controller.get() != 0;
    }


    public boolean isStopped() { return controller.get() == 0; }


    public boolean isRunningFullSpeed()
    {
        return Math.abs(controller.get()) >= maxSpeed;
    }


    /**
     * Gets the absolute speed set on the speed controller as a value between 0  and 1.0.
     *
     * @return the absolute speed set on the speed controller as a value between 0  and 1.0.
     */
    public double getAbsoluteSpeed()
    {
        return Math.abs(controller.get());
    }


    /**
     * Gets the speed percentage, including the '5' sign, as a String formatted as follows:
     * <ol>
     * <li>A forward speed is prefixed with 'F:'. For example: 'F:55%' </li>
     * <li>A reverse speed is prefixed with 'R:'. For example: 'R:10%' </li>
     * <li>Stopped (i.e. speed of 0) is returned as the String 'STOPPED'</li>
     * </ol>
     * This is primarily meant for logging and/or smart dashboard output.
     *
     * @return the speed percentage as a formatted string
     */
    public String getSpeedPercentage()
    {
        if (controller.get() == 0)
        {
            return "STOPPED";
        }
        //noinspection ConditionalExpressionWithIdenticalBranches
        String prefix = isForward() ? "F:" : "R:";
        return prefix + MathUtils.toPercentage(controller.get());
    }


    public boolean isForward()
    {
        return invert ? (controller.get() < 0) : (controller.get() > 0);
    }

    public Direction getDirection()
    {
        if (isStopped())
        {
            return Direction.Stopped;
        }
        else if (isForward())
        {
            return Direction.Forward;
        }
        else
        {
            return Direction.Reverse;
        }
    }


    /**
     * Makes use of a PID Controller's output by calling the
     * {@link edu.wpi.first.wpilibj.SpeedController#pidWrite(double) pidWrite(double)}
     * method on the SpeedController.
     *
     * @param pidOutput the value to write
     */
    @Override
    public void pidWrite(double pidOutput)
    {
        controller.pidWrite(pidOutput);
    }





    // @formatter:off
    public void setForwardSpeed(double speed) { setSpeed(speed); }
    public void setForward01Percent() { setSpeed(0.01); }
    public void setForward02Percent() { setSpeed(0.02); }
    public void setForward03Percent() { setSpeed(0.03); }
    public void setForward04Percent() { setSpeed(0.04); }
    public void setForward05Percent() { setSpeed(0.05); }
    public void setForward06Percent() { setSpeed(0.06); }
    public void setForward07Percent() { setSpeed(0.07); }
    public void setForward08Percent() { setSpeed(0.08); }
    public void setForward09Percent() { setSpeed(0.09); }
    public void setForward10Percent() { setSpeed(0.10); }
    public void setForward11Percent() { setSpeed(0.11); }
    public void setForward12Percent() { setSpeed(0.12); }
    public void setForward13Percent() { setSpeed(0.13); }
    public void setForward14Percent() { setSpeed(0.14); }
    public void setForward15Percent() { setSpeed(0.15); }
    public void setForward16Percent() { setSpeed(0.16); }
    public void setForward17Percent() { setSpeed(0.17); }
    public void setForward18Percent() { setSpeed(0.18); }
    public void setForward19Percent() { setSpeed(0.19); }
    public void setForward20Percent() { setSpeed(0.20); }
    public void setForward21Percent() { setSpeed(0.21); }
    public void setForward22Percent() { setSpeed(0.22); }
    public void setForward23Percent() { setSpeed(0.23); }
    public void setForward24Percent() { setSpeed(0.24); }
    public void setForward25Percent() { setSpeed(0.25); }
    public void setForward26Percent() { setSpeed(0.26); }
    public void setForward27Percent() { setSpeed(0.27); }
    public void setForward28Percent() { setSpeed(0.28); }
    public void setForward29Percent() { setSpeed(0.29); }
    public void setForward30Percent() { setSpeed(0.30); }
    public void setForward31Percent() { setSpeed(0.31); }
    public void setForward32Percent() { setSpeed(0.32); }
    public void setForward33Percent() { setSpeed(0.33); }
    public void setForward34Percent() { setSpeed(0.34); }
    public void setForward35Percent() { setSpeed(0.35); }
    public void setForward36Percent() { setSpeed(0.36); }
    public void setForward37Percent() { setSpeed(0.37); }
    public void setForward38Percent() { setSpeed(0.38); }
    public void setForward39Percent() { setSpeed(0.39); }
    public void setForward40Percent() { setSpeed(0.40); }
    public void setForward41Percent() { setSpeed(0.41); }
    public void setForward42Percent() { setSpeed(0.42); }
    public void setForward43Percent() { setSpeed(0.43); }
    public void setForward44Percent() { setSpeed(0.44); }
    public void setForward45Percent() { setSpeed(0.45); }
    public void setForward46Percent() { setSpeed(0.46); }
    public void setForward47Percent() { setSpeed(0.47); }
    public void setForward48Percent() { setSpeed(0.48); }
    public void setForward49Percent() { setSpeed(0.49); }
    public void setForward50Percent() { setSpeed(0.50); }
    public void setForward51Percent() { setSpeed(0.51); }
    public void setForward52Percent() { setSpeed(0.52); }
    public void setForward53Percent() { setSpeed(0.53); }
    public void setForward54Percent() { setSpeed(0.54); }
    public void setForward55Percent() { setSpeed(0.55); }
    public void setForward56Percent() { setSpeed(0.56); }
    public void setForward57Percent() { setSpeed(0.57); }
    public void setForward58Percent() { setSpeed(0.58); }
    public void setForward59Percent() { setSpeed(0.59); }
    public void setForward60Percent() { setSpeed(0.60); }
    public void setForward61Percent() { setSpeed(0.61); }
    public void setForward62Percent() { setSpeed(0.62); }
    public void setForward63Percent() { setSpeed(0.63); }
    public void setForward64Percent() { setSpeed(0.64); }
    public void setForward65Percent() { setSpeed(0.65); }
    public void setForward66Percent() { setSpeed(0.66); }
    public void setForward67Percent() { setSpeed(0.67); }
    public void setForward68Percent() { setSpeed(0.68); }
    public void setForward69Percent() { setSpeed(0.69); }
    public void setForward70Percent() { setSpeed(0.70); }
    public void setForward71Percent() { setSpeed(0.71); }
    public void setForward72Percent() { setSpeed(0.72); }
    public void setForward73Percent() { setSpeed(0.73); }
    public void setForward74Percent() { setSpeed(0.74); }
    public void setForward75Percent() { setSpeed(0.75); }
    public void setForward76Percent() { setSpeed(0.76); }
    public void setForward77Percent() { setSpeed(0.77); }
    public void setForward78Percent() { setSpeed(0.78); }
    public void setForward79Percent() { setSpeed(0.79); }
    public void setForward80Percent() { setSpeed(0.80); }
    public void setForward81Percent() { setSpeed(0.81); }
    public void setForward82Percent() { setSpeed(0.82); }
    public void setForward83Percent() { setSpeed(0.83); }
    public void setForward84Percent() { setSpeed(0.84); }
    public void setForward85Percent() { setSpeed(0.85); }
    public void setForward86Percent() { setSpeed(0.86); }
    public void setForward87Percent() { setSpeed(0.87); }
    public void setForward88Percent() { setSpeed(0.88); }
    public void setForward89Percent() { setSpeed(0.89); }
    public void setForward90Percent() { setSpeed(0.90); }
    public void setForward91Percent() { setSpeed(0.91); }
    public void setForward92Percent() { setSpeed(0.92); }
    public void setForward93Percent() { setSpeed(0.93); }
    public void setForward94Percent() { setSpeed(0.94); }
    public void setForward95Percent() { setSpeed(0.95); }
    public void setForward96Percent() { setSpeed(0.96); }
    public void setForward97Percent() { setSpeed(0.97); }
    public void setForward98Percent() { setSpeed(0.98); }
    public void setForward99Percent() { setSpeed(0.99); }
    public void setForward100Percent() { setSpeed(1.0); }
    public void setForwardFull() { setSpeed(1.0); }

    public void setReverseSpeed(double speed) { setSpeed(-speed); }
    public void setReverse01Percent() { setSpeed(-0.01); }
    public void setReverse02Percent() { setSpeed(-0.02); }
    public void setReverse03Percent() { setSpeed(-0.03); }
    public void setReverse04Percent() { setSpeed(-0.04); }
    public void setReverse05Percent() { setSpeed(-0.05); }
    public void setReverse06Percent() { setSpeed(-0.06); }
    public void setReverse07Percent() { setSpeed(-0.07); }
    public void setReverse08Percent() { setSpeed(-0.08); }
    public void setReverse09Percent() { setSpeed(-0.09); }
    public void setReverse10Percent() { setSpeed(-0.10); }
    public void setReverse11Percent() { setSpeed(-0.11); }
    public void setReverse12Percent() { setSpeed(-0.12); }
    public void setReverse13Percent() { setSpeed(-0.13); }
    public void setReverse14Percent() { setSpeed(-0.14); }
    public void setReverse15Percent() { setSpeed(-0.15); }
    public void setReverse16Percent() { setSpeed(-0.16); }
    public void setReverse17Percent() { setSpeed(-0.17); }
    public void setReverse18Percent() { setSpeed(-0.18); }
    public void setReverse19Percent() { setSpeed(-0.19); }
    public void setReverse20Percent() { setSpeed(-0.20); }
    public void setReverse21Percent() { setSpeed(-0.21); }
    public void setReverse22Percent() { setSpeed(-0.22); }
    public void setReverse23Percent() { setSpeed(-0.23); }
    public void setReverse24Percent() { setSpeed(-0.24); }
    public void setReverse25Percent() { setSpeed(-0.25); }
    public void setReverse26Percent() { setSpeed(-0.26); }
    public void setReverse27Percent() { setSpeed(-0.27); }
    public void setReverse28Percent() { setSpeed(-0.28); }
    public void setReverse29Percent() { setSpeed(-0.29); }
    public void setReverse30Percent() { setSpeed(-0.30); }
    public void setReverse31Percent() { setSpeed(-0.31); }
    public void setReverse32Percent() { setSpeed(-0.32); }
    public void setReverse33Percent() { setSpeed(-0.33); }
    public void setReverse34Percent() { setSpeed(-0.34); }
    public void setReverse35Percent() { setSpeed(-0.35); }
    public void setReverse36Percent() { setSpeed(-0.36); }
    public void setReverse37Percent() { setSpeed(-0.37); }
    public void setReverse38Percent() { setSpeed(-0.38); }
    public void setReverse39Percent() { setSpeed(-0.39); }
    public void setReverse40Percent() { setSpeed(-0.40); }
    public void setReverse41Percent() { setSpeed(-0.41); }
    public void setReverse42Percent() { setSpeed(-0.42); }
    public void setReverse43Percent() { setSpeed(-0.43); }
    public void setReverse44Percent() { setSpeed(-0.44); }
    public void setReverse45Percent() { setSpeed(-0.45); }
    public void setReverse46Percent() { setSpeed(-0.46); }
    public void setReverse47Percent() { setSpeed(-0.47); }
    public void setReverse48Percent() { setSpeed(-0.48); }
    public void setReverse49Percent() { setSpeed(-0.49); }
    public void setReverse50Percent() { setSpeed(-0.50); }
    public void setReverse51Percent() { setSpeed(-0.51); }
    public void setReverse52Percent() { setSpeed(-0.52); }
    public void setReverse53Percent() { setSpeed(-0.53); }
    public void setReverse54Percent() { setSpeed(-0.54); }
    public void setReverse55Percent() { setSpeed(-0.55); }
    public void setReverse56Percent() { setSpeed(-0.56); }
    public void setReverse57Percent() { setSpeed(-0.57); }
    public void setReverse58Percent() { setSpeed(-0.58); }
    public void setReverse59Percent() { setSpeed(-0.59); }
    public void setReverse60Percent() { setSpeed(-0.60); }
    public void setReverse61Percent() { setSpeed(-0.61); }
    public void setReverse62Percent() { setSpeed(-0.62); }
    public void setReverse63Percent() { setSpeed(-0.63); }
    public void setReverse64Percent() { setSpeed(-0.64); }
    public void setReverse65Percent() { setSpeed(-0.65); }
    public void setReverse66Percent() { setSpeed(-0.66); }
    public void setReverse67Percent() { setSpeed(-0.67); }
    public void setReverse68Percent() { setSpeed(-0.68); }
    public void setReverse69Percent() { setSpeed(-0.69); }
    public void setReverse70Percent() { setSpeed(-0.70); }
    public void setReverse71Percent() { setSpeed(-0.71); }
    public void setReverse72Percent() { setSpeed(-0.72); }
    public void setReverse73Percent() { setSpeed(-0.73); }
    public void setReverse74Percent() { setSpeed(-0.74); }
    public void setReverse75Percent() { setSpeed(-0.75); }
    public void setReverse76Percent() { setSpeed(-0.76); }
    public void setReverse77Percent() { setSpeed(-0.77); }
    public void setReverse78Percent() { setSpeed(-0.78); }
    public void setReverse79Percent() { setSpeed(-0.79); }
    public void setReverse80Percent() { setSpeed(-0.80); }
    public void setReverse81Percent() { setSpeed(-0.81); }
    public void setReverse82Percent() { setSpeed(-0.82); }
    public void setReverse83Percent() { setSpeed(-0.83); }
    public void setReverse84Percent() { setSpeed(-0.84); }
    public void setReverse85Percent() { setSpeed(-0.85); }
    public void setReverse86Percent() { setSpeed(-0.86); }
    public void setReverse87Percent() { setSpeed(-0.87); }
    public void setReverse88Percent() { setSpeed(-0.88); }
    public void setReverse89Percent() { setSpeed(-0.89); }
    public void setReverse90Percent() { setSpeed(-0.90); }
    public void setReverse91Percent() { setSpeed(-0.91); }
    public void setReverse92Percent() { setSpeed(-0.92); }
    public void setReverse93Percent() { setSpeed(-0.93); }
    public void setReverse94Percent() { setSpeed(-0.94); }
    public void setReverse95Percent() { setSpeed(-0.95); }
    public void setReverse96Percent() { setSpeed(-0.96); }
    public void setReverse97Percent() { setSpeed(-0.97); }
    public void setReverse98Percent() { setSpeed(-0.98); }
    public void setReverse99Percent() { setSpeed(-0.99); }
    public void setReverse100Percent() { setSpeed(-1.0); }
    public void setReverseFull() { setSpeed(-1.0); }




    public String increaseForwardSpeedOnePercent() { return increaseForwardSpeed(.01); }
    public String increaseForwardSpeedThreePercent() { return increaseForwardSpeed(.03); }
    public String increaseForwardSpeedFivePercent() { return increaseForwardSpeed(.05); }
    public String increaseForwardSpeedTenPercent() { return increaseForwardSpeed(.1); }
    public String increaseForwardSpeedFifteenPercent() { return increaseForwardSpeed(.15); }
    public String increaseForwardSpeedTwentyPercent() { return increaseForwardSpeed(.20); }
    public String increaseForwardSpeedTwentyFivePercent() { return increaseForwardSpeed(.25); }
    public String increaseForwardSpeedThirtyPercent() { return increaseForwardSpeed(.30); }
    public String increaseForwardSpeedThirtyFivePercent() { return increaseForwardSpeed(.35); }
    public String increaseForwardSpeedFortyPercent() { return increaseForwardSpeed(.40); }
    public String increaseForwardSpeedFortyFivePercent() { return increaseForwardSpeed(.45); }
    public String increaseForwardSpeedFiftyPercent() { return increaseForwardSpeed(.50); }
    public String increaseForwardSpeedSixtyPercent() { return increaseForwardSpeed(.60); }
    public String increaseForwardSpeedSeventyPercent() { return increaseForwardSpeed(.70); }
    public String increaseForwardSpeedSeventyFivePercent() { return increaseForwardSpeed(.75); }
    public String increaseForwardSpeedEightyPercent() { return increaseForwardSpeed(.80); }
    public String increaseForwardSpeedNinetyPercent() { return increaseForwardSpeed(.90); }

    public String decreaseForwardSpeedOnePercent() { return decreaseForwardSpeed(.01); }
    public String decreaseForwardSpeedThreePercent() { return decreaseForwardSpeed(.03); }
    public String decreaseForwardSpeedFivePercent() { return decreaseForwardSpeed(.05); }
    public String decreaseForwardSpeedTenPercent() { return decreaseForwardSpeed(.1); }
    public String decreaseForwardSpeedFifteenPercent() { return decreaseForwardSpeed(.15); }
    public String decreaseForwardSpeedTwentyPercent() { return decreaseForwardSpeed(.20); }
    public String decreaseForwardSpeedTwentyFivePercent() { return decreaseForwardSpeed(.25); }
    public String decreaseForwardSpeedThirtyPercent() { return decreaseForwardSpeed(.30); }
    public String decreaseForwardSpeedThirtyFivePercent() { return decreaseForwardSpeed(.35); }
    public String decreaseForwardSpeedFortyPercent() { return decreaseForwardSpeed(.40); }
    public String decreaseForwardSpeedFortyFivePercent() { return decreaseForwardSpeed(.45); }
    public String decreaseForwardSpeedFiftyPercent() { return decreaseForwardSpeed(.50); }
    public String decreaseForwardSpeedSixtyPercent() { return decreaseForwardSpeed(.60); }
    public String decreaseForwardSpeedSeventyPercent() { return decreaseForwardSpeed(.70); }
    public String decreaseForwardSpeedSeventyFivePercent() { return decreaseForwardSpeed(.75); }
    public String decreaseForwardSpeedEightyPercent() { return decreaseForwardSpeed(.80); }
    public String decreaseForwardSpeedEightyFivePercent() { return decreaseForwardSpeed(.85); }
    public String decreaseForwardSpeedNinetyPercent() { return decreaseForwardSpeed(.90); }
    public String decreaseForwardSpeedNinetyFicePercent() { return decreaseForwardSpeed(.95); }


    // @formatter:on


}
