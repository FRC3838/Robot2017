package frc.team3838.core.hardware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.livewindow.LiveWindowSendable;
import edu.wpi.first.wpilibj.tables.ITable;
import frc.team3838.core.meta.Max;
import frc.team3838.core.meta.Min;



public class LiveWindowSendableMotorOps extends MotorOps implements LiveWindowSendable
{
    private static final Logger logger = LoggerFactory.getLogger(LiveWindowSendableMotorOps.class);

    private LiveWindowSendable liveWindowSendable;

    public LiveWindowSendableMotorOps(SpeedController speedController, boolean invert)
    {
        super(speedController, invert);
        initLiveWindowSendable();
    }




    public LiveWindowSendableMotorOps(SpeedController speedController, boolean invert, double minOrStallSpeed)
    {
        super(speedController, invert, minOrStallSpeed);
        initLiveWindowSendable();
    }


    public LiveWindowSendableMotorOps(SpeedController speedController, boolean invert, @Min(0) @Max(0.99) double minOrStallSpeed, @Min(0.01) @Max(1.0) double maxSpeed)
    {
        super(speedController, invert, minOrStallSpeed, maxSpeed);
        initLiveWindowSendable();
    }


    private void initLiveWindowSendable()
    {
        if (!(getSpeedController() instanceof LiveWindowSendable))
        {
            throw new IllegalArgumentException("Supplied SpeedController does not implement LiveWindowSendable interface... it needs to.");
        }
        this.liveWindowSendable = (LiveWindowSendable) getSpeedController();
    }


    public LiveWindowSendable getSpeedControllerAsLiveWindowSendable()
    {
        return liveWindowSendable;
    }

    @Override
    public void updateTable() { liveWindowSendable.updateTable(); }


    @Override
    public void startLiveWindowMode() { liveWindowSendable.startLiveWindowMode(); }


    @Override
    public void stopLiveWindowMode() {liveWindowSendable.stopLiveWindowMode();}


    @Override
    public void initTable(ITable subTable) { liveWindowSendable.initTable(subTable); }


    @Override
    public ITable getTable() { return liveWindowSendable.getTable(); }


    @Override
    public String getSmartDashboardType() { return liveWindowSendable.getSmartDashboardType(); }

}
