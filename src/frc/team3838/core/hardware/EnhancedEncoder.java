package frc.team3838.core.hardware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.DigitalSource;
import edu.wpi.first.wpilibj.Encoder;
import frc.team3838.core.utils.MathUtils;



public class EnhancedEncoder extends Encoder
{
    private static final Logger logger = LoggerFactory.getLogger(EnhancedEncoder.class);


    public EnhancedEncoder(int channelA, int channelB, boolean reverseDirection)
    {
        super(channelA, channelB, reverseDirection);
    }


    public EnhancedEncoder(int channelA, int channelB)
    {
        super(channelA, channelB);
    }


    public EnhancedEncoder(int channelA, int channelB, boolean reverseDirection, EncodingType encodingType)
    {
        super(channelA, channelB, reverseDirection, encodingType);
    }


    public EnhancedEncoder(int channelA, int channelB, int indexChannel, boolean reverseDirection)
    {
        super(channelA, channelB, indexChannel, reverseDirection);
    }


    public EnhancedEncoder(int channelA, int channelB, int indexChannel)
    {
        super(channelA, channelB, indexChannel);
    }


    public EnhancedEncoder(DigitalSource sourceA, DigitalSource sourceB, boolean reverseDirection)
    {
        super(sourceA, sourceB, reverseDirection);
    }


    public EnhancedEncoder(DigitalSource sourceA, DigitalSource sourceB)
    {
        super(sourceA, sourceB);
    }


    public EnhancedEncoder(DigitalSource sourceA, DigitalSource sourceB, boolean reverseDirection, EncodingType encodingType)
    {
        super(sourceA, sourceB, reverseDirection, encodingType);
    }


    public EnhancedEncoder(DigitalSource sourceA, DigitalSource sourceB, DigitalSource indexSource, boolean reverseDirection)
    {
        super(sourceA, sourceB, indexSource, reverseDirection);
    }


    public EnhancedEncoder(DigitalSource sourceA, DigitalSource sourceB, DigitalSource indexSource)
    {
        super(sourceA, sourceB, indexSource);
    }


    public double getDistanceInFeet() { return getDistance() / 12; }


    public double getDistanceInFeetRounded() { return MathUtils.roundToNPlaces(getDistanceInFeet(), 3); }


    public String getDistanceInFeetAsString() { return MathUtils.formatNumber(getDistanceInFeet(), 3); }


    public double getDistanceInInches() { return getDistance(); }


    public double getDistanceInInchesRounded() { return MathUtils.roundToNPlaces(getDistanceInInches(), 3); }


    public String getDistanceInInchesAsString() { return MathUtils.formatNumber(getDistanceInInches(), 3); }
}
