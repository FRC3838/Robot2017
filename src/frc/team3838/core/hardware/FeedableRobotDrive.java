package frc.team3838.core.hardware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;



public class FeedableRobotDrive extends RobotDrive
{
    private static final Logger logger = LoggerFactory.getLogger(FeedableRobotDrive.class);


    public FeedableRobotDrive(int leftMotorChannel, int rightMotorChannel)
    {
        super(leftMotorChannel, rightMotorChannel);
    }


    public FeedableRobotDrive(int frontLeftMotor, int rearLeftMotor, int frontRightMotor, int rearRightMotor)
    {
        super(frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor);
    }


    public FeedableRobotDrive(SpeedController leftMotor, SpeedController rightMotor)
    {
        super(leftMotor, rightMotor);
    }


    public FeedableRobotDrive(SpeedController frontLeftMotor, SpeedController rearLeftMotor, SpeedController frontRightMotor, SpeedController rearRightMotor)
    {
        super(frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor);
    }

    public void feedMotorSafety()
    {
        if (m_safetyHelper != null)
        {
            m_safetyHelper.feed();
        }
    }
}
