package frc.team3838.core.utils;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import frc.team3838.core.meta.API;



@API
public class PIDSetting
{
    private double P;
    private double I;
    private double D;
//    private double F;


    @API
    public PIDSetting() { }


    @API
    public PIDSetting(double p, double i, double d)
    {
        P = p;
        I = i;
        D = d;
    }

//    @API
//    public PIDSetting(double p, double i, double d, double f)
//    {
//        P = p;
//        I = i;
//        D = d;
//        F = f;
//    }


    @API
    public double getP() { return P; }


    @API
    public void setP(double p) { P = p; }


    @API
    public double getI() { return I; }


    @API
    public void setI(double i) { I = i; }


    @API
    public double getD() { return D; }


    @API
    public void setD(double d) { D = d; }

//    @API
//    public double getF() { return F; }
//
//
//    @API
//    public void setF(double f) { F = f; }


    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("P", P)
            .append("I", I)
            .append("D", D)
//            .append("F", F)
            .toString();
    }
}
