package frc.team3838.core.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.command.Command;



@SuppressWarnings("UnusedDeclaration")
public enum ButtonAction implements ControlAction<Button>
{
    /** Cancel the command when the button is pressed. This is equivalent to the Trigger action {@code cancelWhenActive()}. */
    Button_CancelWhenHeld
        {
            @Override
            public void assign(Button button, Command command)
            {

                logAssignment(button, command, this);
                button.cancelWhenPressed(command);
            }
        },
    /** 
     * Toggles (the running state of) a command whenever the button is pressed (on then off then on). 
     * This is equivalent to the Trigger action {@code toggleWhenActive()}. The toggling is done by the 
     * system checking {@code command.isRunning()} and then calling either {@code command.cancel()} or 
     * {@code command.start()} as appropriate.
     */
    Button_ToggleWhenPressed
        {
            @Override
            public void assign(Button button, Command command)
            {
                logAssignment(button, command, this);
                button.toggleWhenPressed(command);
            }
        },
    /** Starts the given command whenever the button is newly pressed. This is equivalent to the Trigger action {@code whenActive()}. */
    Button_WhenPressed
        {
            @Override
            public void assign(Button button, Command command)
            {
                logAssignment(button, command, this);
                button.whenPressed(command);
            }
        },
    /** Starts the command when the button is released. This is equivalent to the Trigger action {@code whenInactive()}. */
    Button_WhenReleased
        {
            @Override
            public void assign(Button button, Command command)
            {
                logAssignment(button, command, this);
                button.whenReleased(command);
            }
        },
    /**
     * Constantly starts the given command while the button is held. Command.start() will be called
     * repeatedly while the button is held, and will be canceled when the button is released.
     * This is equivalent to the Trigger action {@code whileActive()}.
     */
    Button_WhileHeld
        {
            @Override
            public void assign(Button button, Command command)
            {
                logAssignment(button, command, this);
                button.whileHeld(command);
            }
        },

    
    /*
         _____    _                       _      _   _             
        |_   _| _(_)__ _ __ _ ___ _ _    /_\  __| |_(_)___ _ _  ___
          | || '_| / _` / _` / -_) '_|  / _ \/ _|  _| / _ \ ' \(_-<
          |_||_| |_\__, \__, \___|_|   /_/ \_\__|\__|_\___/_||_/__/
                   |___/|___/                                      
    */
    

    /** <strong>Trigger action that</strong> Cancels a command when the trigger becomes active. */
    Trigger_CancelWhenActive
    {
        @Override
        public void assign (Button trigger, Command command)
        {
            logAssignment(trigger, command, this);
            trigger.cancelWhenActive(command);
        }
    },
    
    /** 
     * <strong>Trigger action that</strong> Toggles (the running state of) a command when the trigger becomes active. 
     * The toggling is done by the system checking {@code command.isRunning()} and then calling either {@code command.cancel()} or {@code command.start()}
     * as appropriate.
     */
    Trigger_ToggleWhenActive
    {
        @Override
        public void assign (Button trigger, Command command)
        {
            logAssignment(trigger, command, this);
            trigger.toggleWhenActive(command);
        }
    },
    
    /** <strong>Trigger action that</strong> Starts the given command whenever the trigger just becomes active. */
    Trigger_WhenActive
    {
        @Override
        public void assign (Button trigger, Command command)
        {
            logAssignment(trigger, command, this);
            trigger.whenActive(command);
        }
    },
    
    /** <strong>Trigger action that</strong> Starts the command when the trigger becomes inactive. */
    Trigger_WhenInactive
    {
        @Override
        public void assign (Button trigger, Command command)
        {
            logAssignment(trigger, command, this);
            trigger.whenInactive(command);
        }
    },
    
    /** <strong>Trigger action that</strong> Starts the given command whenever the trigger just becomes active. */
    Trigger_WhileActive
    {
        @Override
        public void assign (Button trigger, Command command)
        {
            logAssignment(trigger, command, this);
            trigger.whenActive(command);
        }
    }

    ;
    
    private static final Logger logger = LoggerFactory.getLogger(ButtonAction.class);


    private static void logAssignment(Button button, Command command, ButtonAction action)
    {
        if (logger.isDebugEnabled())
        {
            @SuppressWarnings("CastToConcreteClass")
            final String buttonName = (button instanceof NamedJoystickButton) ? ((NamedJoystickButton) button).getButtonName() : "unnamed";
            logger.debug("Assigning {} {} the command {}::{}", buttonName, action.name(), command.getName(), command.getClass().getName());
        }
    }

}
