package frc.team3838.core.controls;

public enum PovDirection
{
    N_UP(0),
    NE(45),
    E_RIGHT(90),
    SE(135),
    S_DOWN(180),
    SW(225),
    W_LRFT(270),
    NW(315);


    PovDirection(int angle)
    {
        this.angle = angle;
    }


    private final int angle;


    public int getAngle()
    {
        return angle;
    }
}
