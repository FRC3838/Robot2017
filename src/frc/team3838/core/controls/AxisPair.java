package frc.team3838.core.controls;

public interface AxisPair
{
    double getXorLeft();
    double getYorRight();
}
