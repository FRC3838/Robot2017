package frc.team3838.core.controls;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.annotations.VisibleForTesting;

import edu.wpi.first.wpilibj.GamepadBase;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.JoystickBase;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.ActivationButtonStep;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.BuildStep;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.DeadZoneThresholdStep;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.GamepadBaseOptions;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.GenericDirectionalityStep;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.GenericHidOptions;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.HidTypeStep;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.InvertStep;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.JoystickBaseOptions;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.JoystickOptions;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.OutputSignStep;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.ScalingStep;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.XAxisDirectionalityStep;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.XboxControllerOptions;
import frc.team3838.core.controls.ThrottleBuilder.BuilderSteps.YAxisDirectionalityStep;
import frc.team3838.core.meta.API;
import frc.team3838.core.utils.MathUtils;



/**
 * A <a href='https://en.wikipedia.org/wiki/Builder_pattern'>Builder</a> for creating a Throttle. An
 */
@API
public class ThrottleBuilder
{
    private static final Logger logger = LoggerFactory.getLogger(ThrottleBuilder.class);

    public enum Directionality
    {
        Bi, Positive, Negative
    }

    @API
    public static HidTypeStep getBuilder()
    {
        return new Builder();
    }


    @SuppressWarnings("unused")
    public interface BuilderSteps
    {

        @SuppressWarnings("unused")
        interface HidTypeStep
        {
            GenericHidOptions using(GenericHID genericHID);

            JoystickBaseOptions using(JoystickBase joystickBase);

            JoystickOptions using(Joystick joystick);

            GamepadBaseOptions using(GamepadBase gamepadBase);

            XboxControllerOptions using(XboxController xboxController);
        }

        @SuppressWarnings("unused")
        interface GenericHidOptions
        {
            //  GenericHid.getRawAxis(int which)
            GenericDirectionalityStep usingRawAxis(int rawAxis);

            //  GenericHid.getX() & GenericHid.getX(Hand)
            XAxisDirectionalityStep usingXAxis();

            XAxisDirectionalityStep usingXAxis(GenericHID.Hand hand);

            //  GenericHid.getY() & GenericHid.getY(Hand)
            YAxisDirectionalityStep usingYAxis();

            YAxisDirectionalityStep usingYAxis(GenericHID.Hand hand);
        }

        @SuppressWarnings("unused")
        interface JoystickBaseOptions extends GenericHidOptions
        {
            //  JoystickBase.getZ() & JoystickBase.getZ(Hand)
            GenericDirectionalityStep usingZAxis();

            GenericDirectionalityStep usingZAxis(GenericHID.Hand hand);

            //  JoystickBase.getThrottle()
            GenericDirectionalityStep usingThrottle();

            //  JoystickBase.getTwist()
            GenericDirectionalityStep usingTwist();
        }


        @SuppressWarnings("unused")
        interface JoystickOptions extends JoystickBaseOptions
        {
            GenericDirectionalityStep usingAxisType(Joystick.AxisType axisType);
        }

        @SuppressWarnings("unused")
        interface GamepadBaseOptions extends GenericHidOptions
        {

        }

        @SuppressWarnings("unused")
        interface XboxControllerOptions extends GamepadBaseOptions
        {
            GenericDirectionalityStep usingTriggerAxis(GenericHID.Hand hand);
        }


        @SuppressWarnings("unused")
        interface GenericDirectionalityStep
        {
            InvertStep usingBiDirectionality();

            OutputSignStep usingPositiveDirection();

            OutputSignStep usingNegativeDirection();
        }

        @SuppressWarnings("unused")
        interface XAxisDirectionalityStep
        {
            InvertStep usingBiDirectionality();

            OutputSignStep usingRightPositiveDirection();

            OutputSignStep usingLeftNegativeDirection();
        }

        @SuppressWarnings("unused")
        interface YAxisDirectionalityStep
        {
            InvertStep usingBiDirectionality();

            OutputSignStep usingDownPullPositiveDirection();

            OutputSignStep usingUpPushNegativeDirection();
        }

        @SuppressWarnings("unused")
        interface OutputSignStep
        {
            DeadZoneThresholdStep outputPositiveValue();

            DeadZoneThresholdStep outputNegativeValue();
        }


        @SuppressWarnings({"unused", "SameParameterValue"})
        interface InvertStep
        {
            DeadZoneThresholdStep invert(boolean invert);

            DeadZoneThresholdStep usingInvertedOutput();

            /** Do NOT invert the output, thus sending the value reported by the joystick. */
            DeadZoneThresholdStep usingNonInvertedOutput();
        }




        @SuppressWarnings({"unused", "SameParameterValue"})
        interface DeadZoneThresholdStep
        {
            ScalingStep withoutDeadZone();
            ScalingStep withDeadZoneThreshold(double deadZoneThreshold);
        }

        @SuppressWarnings({"unused", "SameParameterValue"})
        interface ScalingStep
        {
            ActivationButtonStep withoutScaling();

            /**
             * Configures the throttle ro use absolute scaling, such that the absolute range of the output will be between
             * the provided values. For example, if given the values 0, 0.5 for a bidirectional throttle, the output
             * will range from -0.5 to 0.5. So if the joystick value is -0.5, the output valuer will be scaled to -0.25.
             * A joystick value of 0.75 would be scaled to 0.375. A joy stick value of 1.0 would be scaled to 0.5.
             * @param absoluteMin the (positive) absolute min value
             * @param absoluteMax the (positive) absolute max value
             * @return the next step
             */
            ActivationButtonStep withAbsoluteScaling(double absoluteMin, double absoluteMax);
            ActivationButtonStep withAbsoluteScaling(double absoluteMin, double absoluteMax, double availableRangeMin, double availableRangeMax);
//            @SuppressWarnings("SameParameterValue")
            ActivationButtonStep withFullRangeScaling(double scaledRangeMin, double scaledRangeMax);
            ActivationButtonStep withFullRangeScaling(double scaledRangeMin, double scaledRangeMax, double availableRangeMin, double availableRangeMax);
        }

        @SuppressWarnings({"unused", "SameParameterValue"})
        interface ActivationButtonStep
        {
            //TODO add JoyStickBase.getTrigger option  and JoyStick.getButton(ButtonType) - likely need to move to ealier in the process, or use marker interfaces in the ablve chain

            BuildStep withoutActivationButton();
            BuildStep withWhileHeldButton(Trigger button);
            BuildStep withWhileHeldButton(int rawButtonNumber);
            BuildStep withWhileHeldButton(GenericHID hid, int rawButtonNumber);

            BuildStep withWhileHeldButton(Trigger button, Directionality onlyNeedActivationForThisDirection);
            BuildStep withWhileHeldButton(int rawButtonNumber, Directionality onlyNeedActivationForThisDirection);
            BuildStep withWhileHeldButton(GenericHID hid, int rawButtonNumber, Directionality onlyNeedActivationForThisDirection);



//            BuildStep withTogglingButton(Button button, boolean initiallyActivated);
//            BuildStep withTogglingButton(int rawButtonNumber, boolean initiallyActivated);
//            BuildStep withTogglingButton(GenericHID hid,  int rawButtonNumber, boolean initiallyActivated);
        }


        @SuppressWarnings("unused")
        interface BuildStep
        {
            Throttle build();
        }
    }


    private static class Builder implements BuilderSteps.HidTypeStep,
                                            BuilderSteps.GenericHidOptions,
                                            BuilderSteps.JoystickBaseOptions,
                                            BuilderSteps.JoystickOptions,
                                            BuilderSteps.GamepadBaseOptions,
                                            BuilderSteps.XboxControllerOptions,
                                            BuilderSteps.GenericDirectionalityStep,
                                            BuilderSteps.XAxisDirectionalityStep,
                                            BuilderSteps.YAxisDirectionalityStep,
                                            BuilderSteps.OutputSignStep,
                                            BuilderSteps.InvertStep,
                                            BuilderSteps.DeadZoneThresholdStep,
                                            BuilderSteps.ScalingStep,
                                            BuilderSteps.ActivationButtonStep,
                                            BuilderSteps.BuildStep

    {


        private enum Mode
        {
            RawAxis, Axis, AxisHand, AxisType, JoystickThrottle, Twist, TriggerAxis
        }

        private enum Axis
        {
            X, Y, Z
        }



        private enum ScalingType {None, Absolute, Full}

        private enum ActivationButtonType {None, Modifier, Toggling}

        private GenericHID genericHID;
        private JoystickBase joystickBase;
        private Joystick joystick;
        @SuppressWarnings("unused")
        private GamepadBase gamepadBase;
        private XboxController xboxController;

        private Mode mode;

        private Integer rawAxis;
        private Axis axis;
        private GenericHID.Hand hand;
        private Joystick.AxisType axisType;

        private double deadZoneThreshold = 0;

        private Directionality directionality;

        private int invertFactor;

        private ScalingType scalingType;
        private double scalingRangeMin = -1;
        private double scalingRangeMax = 1;
        private double availableRangeMin = -1;
        private double availableRangeMax = 1;

        private Trigger activationButton;
        private int activationRawButtonNumber;
        private GenericHID activationHid;
        private Directionality activationDirectionality = Directionality.Bi;

        private ActivationButtonType activationButtonType = ActivationButtonType.None;



        @Override
        public GenericHidOptions using(@Nonnull GenericHID genericHID)
        {
            //noinspection ConstantConditions
            if (genericHID == null) { throw new IllegalArgumentException("genericHID cannot be null when passed into ThrottleBuilder."); }
            this.genericHID = genericHID;
            return this;
        }


        @Override
        public JoystickBaseOptions using(@Nonnull JoystickBase joystickBase)
        {
            //noinspection ConstantConditions
            if (joystickBase == null) { throw new IllegalArgumentException("joystickBase cannot be null when passed into ThrottleBuilder."); }
            this.genericHID = joystickBase;
            this.joystickBase = joystickBase;
            return this;
        }


        @Override
        public JoystickOptions using(@Nonnull Joystick joystick)
        {
            //noinspection ConstantConditions
            if (joystick == null) { throw new IllegalArgumentException("joystick cannot be null when passed into ThrottleBuilder."); }
            this.genericHID = joystick;
            this.joystickBase = joystick;
            this.joystick = joystick;
            return this;
        }


        @Override
        public GamepadBaseOptions using(@Nonnull GamepadBase gamepadBase)
        {
            //noinspection ConstantConditions
            if (gamepadBase == null) { throw new IllegalArgumentException("gamepadBase cannot be null when passed into ThrottleBuilder."); }
            this.genericHID = gamepadBase;
            this.gamepadBase = gamepadBase;
            return this;
        }


        @Override
        public XboxControllerOptions using(@Nonnull XboxController xboxController)
        {
            //noinspection ConstantConditions
            if (xboxController == null) { throw new IllegalArgumentException("xboxController cannot be null when passed into ThrottleBuilder."); }
            this.genericHID = xboxController;
            this.gamepadBase = xboxController;
            this.xboxController = xboxController;
            return this;
        }


        @Override
        public GenericDirectionalityStep usingRawAxis(int rawAxis)
        {
            this.mode = Mode.RawAxis;
            this.rawAxis = rawAxis;
            return this;
        }


        @Override
        public XAxisDirectionalityStep usingXAxis()
        {
            this.mode = Mode.Axis;
            this.axis = Axis.X;
            return this;
        }


        @Override
        public XAxisDirectionalityStep usingXAxis(GenericHID.Hand hand)
        {
            this.mode = Mode.AxisHand;
            this.axis = Axis.X;
            this.hand = hand;
            return this;
        }


        @Override
        public YAxisDirectionalityStep usingYAxis()
        {
            this.mode = Mode.Axis;
            this.axis = Axis.Y;
            return this;
        }


        @Override
        public YAxisDirectionalityStep usingYAxis(GenericHID.Hand hand)
        {
            this.mode = Mode.AxisHand;
            this.axis = Axis.Y;
            this.hand = hand;
            return this;
        }


        @Override
        public GenericDirectionalityStep usingZAxis()
        {
            this.mode = Mode.Axis;
            this.axis = Axis.Z;
            return this;
        }


        @Override
        public GenericDirectionalityStep usingZAxis(GenericHID.Hand hand)
        {
            this.mode = Mode.AxisHand;
            this.axis = Axis.Z;
            this.hand = hand;
            return this;
        }


        @Override
        public GenericDirectionalityStep usingThrottle()
        {
            this.mode = Mode.JoystickThrottle;
            return this;
        }


        @Override
        public GenericDirectionalityStep usingTwist()
        {
            this.mode = Mode.Twist;
            return this;
        }


        @Override
        public GenericDirectionalityStep usingAxisType(Joystick.AxisType axisType)
        {
            this.mode = Mode.AxisType;
            this.axisType = axisType;
            return this;
        }


        @Override
        public GenericDirectionalityStep usingTriggerAxis(GenericHID.Hand hand)
        {
            this.mode = Mode.TriggerAxis;
            this.hand = hand;
            return this;
        }


        @Override
        public InvertStep usingBiDirectionality()
        {
            this.directionality = Directionality.Bi;
            return this;
        }


        @Override
        public OutputSignStep usingPositiveDirection()
        {
            this.directionality = Directionality.Positive;
            return this;
        }


        @Override
        public OutputSignStep usingNegativeDirection()
        {
            this.directionality = Directionality.Negative;
            return this;
        }


        @Override
        public OutputSignStep usingRightPositiveDirection()
        {
            return usingPositiveDirection();
        }


        @Override
        public OutputSignStep usingLeftNegativeDirection()
        {
            return usingNegativeDirection();
        }


        @Override
        public OutputSignStep usingDownPullPositiveDirection()
        {
            return usingPositiveDirection();
        }


        @Override
        public OutputSignStep usingUpPushNegativeDirection()
        {
            return usingNegativeDirection();
        }


        @Override
        public DeadZoneThresholdStep outputPositiveValue()
        {
            if (directionality == Directionality.Negative)
            {
                invertFactor = -1;
            }
            else
            {
                invertFactor = 1;
            }
            return this;
        }


        @Override
        public DeadZoneThresholdStep outputNegativeValue()
        {
            if (directionality == Directionality.Positive)
            {
                invertFactor = -1;
            }
            else
            {
                invertFactor = 1;
            }
            return this;
        }


        @Override
        public DeadZoneThresholdStep invert(boolean invert)
        {
            this.invertFactor = invert ? -1 : 1;
            return this;
        }


        @Override
        public DeadZoneThresholdStep usingInvertedOutput()
        {
            return invert(true);
        }


        @Override
        public DeadZoneThresholdStep usingNonInvertedOutput()
        {
            return invert(false);
        }


        @Override
        public ScalingStep withoutDeadZone()
        {
            this.deadZoneThreshold = 0;
            return this;
        }


        @Override
        public ScalingStep withDeadZoneThreshold(double deadZoneThreshold)
        {
            this.deadZoneThreshold = Math.abs(deadZoneThreshold);
            return this;
        }


        @Override
        public ActivationButtonStep withoutScaling()
        {
            this.scalingType = ScalingType.None;
            return this;
        }


        @Override
        public ActivationButtonStep withAbsoluteScaling(double absoluteMin, double absoluteMax)
        {
            return withAbsoluteScaling(absoluteMin, absoluteMax, 0, 1);
        }


        @Override
        public ActivationButtonStep withAbsoluteScaling(double absoluteMin, double absoluteMax, double availableRangeMin, double availableRangeMax)
        {
            this.scalingType = ScalingType.Absolute;
            this.scalingRangeMin = absoluteMin;
            this.scalingRangeMax = absoluteMax;
            this.availableRangeMin = availableRangeMin;
            this.availableRangeMax = availableRangeMax;
            return this;
        }


        @Override
        public ActivationButtonStep withFullRangeScaling(double scaledRangeMin, double scaledRangeMax)
        {
            return withFullRangeScaling(scaledRangeMin, scaledRangeMax, -1, 1);
        }


        @Override
        public ActivationButtonStep withFullRangeScaling(double scaledRangeMin, double scaledRangeMax, double availableRangeMin, double availableRangeMax)
        {
            if (scaledRangeMax <= scaledRangeMin)
            {
                throw new IllegalArgumentException("Scaling 'scaledRangeMax' value must be greater than 'scaledRangeMin' value");
            }

            if (availableRangeMax <= availableRangeMin)
            {
                throw new IllegalArgumentException("Scaling 'availableRangeMax' value must be greater than 'availableRangeMin' value");
            }

            this.scalingType = ScalingType.Full;
            this.scalingRangeMin = scaledRangeMin;
            this.scalingRangeMax = scaledRangeMax;
            this.availableRangeMin = availableRangeMin;
            this.availableRangeMax = availableRangeMax;
            return this;
        }


        @Override
        public BuildStep withoutActivationButton()
        {
            activationButtonType = ActivationButtonType.None;
            return this;
        }


        @Override
        public BuildStep withWhileHeldButton(Trigger button)
        {
            activationButtonType = ActivationButtonType.Modifier;
            activationButton = button;
            activationDirectionality = Directionality.Bi;
            return this;
        }


        @Override
        public BuildStep withWhileHeldButton(int rawButtonNumber)
        {
            activationButtonType = ActivationButtonType.Modifier;
            activationHid = genericHID;
            activationRawButtonNumber = rawButtonNumber;
            activationDirectionality = Directionality.Bi;
            return this;
        }


        @Override
        public BuildStep withWhileHeldButton(GenericHID hid, int rawButtonNumber)
        {
            activationButtonType = ActivationButtonType.Modifier;
            activationHid = hid;
            activationRawButtonNumber = rawButtonNumber;
            activationDirectionality = Directionality.Bi;
            return this;
        }


        @Override
        public BuildStep withWhileHeldButton(Trigger button, Directionality onlyNeedActivationForThisDirection)
        {
            activationButtonType = ActivationButtonType.Modifier;
            activationButton = button;
            activationDirectionality = onlyNeedActivationForThisDirection;
            return this;
        }


        @Override
        public BuildStep withWhileHeldButton(int rawButtonNumber, Directionality onlyNeedActivationForThisDirection)
        {
            activationButtonType = ActivationButtonType.Modifier;
            activationHid = genericHID;
            activationRawButtonNumber = rawButtonNumber;
            activationDirectionality = onlyNeedActivationForThisDirection;
            return this;
        }


        @Override
        public BuildStep withWhileHeldButton(GenericHID hid, int rawButtonNumber, Directionality onlyNeedActivationForThisDirection)
        {
            activationButtonType = ActivationButtonType.Modifier;
            activationHid = hid;
            activationRawButtonNumber = rawButtonNumber;
            activationDirectionality = onlyNeedActivationForThisDirection;
            return this;
        }



//
//
//        @Override
//        public BuildStep withTogglingButton(Button button, boolean initiallyActivated)
//        {
//            activationButtonType = ActivationButtonType.Toggling;
//            activationButton = button;
//            activationTogglingIsActive = initiallyActivated;
//            return this;
//        }
//
//
//        @Override
//        public BuildStep withTogglingButton(int rawButtonNumber, boolean initiallyActivated)
//        {
//            activationButtonType = ActivationButtonType.Toggling;
//            activationHid = genericHID;
//            activationRawButtonNumber = rawButtonNumber;
//            activationTogglingIsActive = initiallyActivated;
//            return this;
//        }
//
//
//        @Override
//        public BuildStep withTogglingButton(GenericHID hid, int rawButtonNumber, boolean initiallyActivated)
//        {
//            activationButtonType = ActivationButtonType.Toggling;
//            activationHid = hid;
//            activationRawButtonNumber = rawButtonNumber;
//            activationTogglingIsActive = initiallyActivated;
//            return this;
//        }


        @Override
        public Throttle build()
        {
            switch (this.mode)
            {
                case RawAxis:
                    return () -> calc(genericHID.getRawAxis(rawAxis)) * invertFactor;
                case Axis:
                {
                    switch (this.axis)
                    {
                        case X:
                            return () -> calc(genericHID.getX()) * invertFactor;
                        case Y:
                            return () -> calc(genericHID.getY()) * invertFactor;
                        case Z:
                            return () -> calc(joystickBase.getZ()) * invertFactor;
                    }
                }
                case AxisHand:
                    switch (this.axis)
                    {
                        case X:
                            return () -> calc(genericHID.getX(hand)) * invertFactor;
                        case Y:
                            return () -> calc(genericHID.getY(hand)) * invertFactor;
                        case Z:
                            return () -> calc(joystickBase.getZ(hand)) * invertFactor;
                    }
                case AxisType:
                    return () -> calc(joystick.getAxis(axisType)) * invertFactor;
                case JoystickThrottle:
                    return () -> calc(joystick.getThrottle()) * invertFactor;
                case Twist:
                    return () -> calc(joystick.getTwist()) * invertFactor;
                case TriggerAxis:
                    return () -> calc(xboxController.getTriggerAxis(hand)) * invertFactor;

            }

            logger.error("Unhandled case in ThrottleBuilder. Builder state: {}", toString());
            throw new IllegalStateException("Unhandled case in ThrottleBuilder. Builder state: " + toString());
        }




        @VisibleForTesting
        protected double calc(double d)
        {
            if (d == 0) {return d;}

            if (Math.abs(d) < deadZoneThreshold)
            {
                return 0;
            }

            switch (activationButtonType)
            {
                case Modifier:

                    boolean checkActivation;
                    switch (activationDirectionality)
                    {
                        case Positive:
                            checkActivation = MathUtils.isPositive(d);
                            break;
                        case Negative:
                            checkActivation = MathUtils.isNegative(d);
                            break;
                        case Bi:
                            checkActivation = true;
                            break;
                        default:
                            checkActivation = false;
                    }

                    if (checkActivation)
                    {
                        if (activationButton != null)
                        {

                            if (!activationButton.get())
                            {
                                return 0;
                            }
                        }
                        else
                        {
                            if (!activationHid.getRawButton(activationRawButtonNumber))
                            {
                                return 0;
                            }
                        }
                    }
                case None:
                case Toggling: // Not yet implemented
            }

            final boolean wasNegative = MathUtils.isNegative(d);

            // TODO - scale the value to the usable range of the joystick?

            switch (scalingType)
            {
                case None:
                    // no op
                    break;
                case Absolute:

                    d = MathUtils.scaleRange(Math.abs(d), availableRangeMin, availableRangeMax, scalingRangeMin, scalingRangeMax);
                    if (wasNegative) { d = -d; }
                    break;
                case Full:
                    d = MathUtils.scaleRange(d, availableRangeMin, availableRangeMax, scalingRangeMin, scalingRangeMax);
            }


            switch (directionality)
            {
                case Positive:
                    //return MathUtils.isPositive(value)
                    return (d == Math.abs(d)) ? d : 0;
                case Negative:
                    return (d == -Math.abs(d)) ? d : 0;
                case Bi:
                default:
                    return d;
            }
        }


        @Override
        public String toString()
        {
            return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
                .append("mode", mode)
                .append("rawAxis", rawAxis)
                .append("axis", axis)
                .append("hand", hand)
                .append("axisType", axisType)
                .append("deadZoneThreshold", deadZoneThreshold)
                .append("directionality", directionality)
                .append("invertFactor", invertFactor)
                .append("scalingType", scalingType)
                .append("scalingRangeMin", scalingRangeMin)
                .append("scalingRangeMax", scalingRangeMax)
                .append("availableRangeMin", availableRangeMin)
                .append("availableRangeMax", availableRangeMax)
                .append("genericHID", genericHID)
                .append("joystickBase", joystickBase)
                .append("joystick", joystick)
                .append("gamepadBase", gamepadBase)
                .append("xboxController", xboxController)
                .toString();
        }
    }

}
