package frc.team3838.core.controls;

import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.GenericHID;
import frc.team3838.core.meta.API;



@API
public class HidBasedAxisPair implements AxisPair
{
    private static final int USE_DIRECT_AXIS = -1;
    @Nonnull
    private final GenericHID hid;
    private final int xOrLeftRawAxis;
    private final int yOrRightRawAxis;


    public HidBasedAxisPair(@Nonnull GenericHID hid)
    {
        this.hid = hid;
        this.xOrLeftRawAxis = USE_DIRECT_AXIS;
        this.yOrRightRawAxis = USE_DIRECT_AXIS;
    }
    public HidBasedAxisPair(@Nonnull GenericHID hid, int xOrLeftRawAxis, int yOrRightRawAxis)
    {
        this.hid = hid;
        this.xOrLeftRawAxis = xOrLeftRawAxis;
        this.yOrRightRawAxis = yOrRightRawAxis;
    }


    @Override
    public double getXorLeft()
    {
        return (xOrLeftRawAxis == USE_DIRECT_AXIS) ? hid.getX() : hid.getRawAxis(xOrLeftRawAxis);
    }


    @Override
    public double getYorRight()
    {
        return (yOrRightRawAxis == USE_DIRECT_AXIS) ? hid.getY() : hid.getRawAxis(yOrRightRawAxis);
    }
}
