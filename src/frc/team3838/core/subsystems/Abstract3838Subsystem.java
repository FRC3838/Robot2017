package frc.team3838.core.subsystems;

import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team3838.core.meta.API;



public abstract class Abstract3838Subsystem extends Subsystem implements I3838Subsystem
{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final Logger LOG = LoggerFactory.getLogger(Abstract3838Subsystem.class);

    protected boolean enabled = Subsystems.checkIfEnabledInRobotMap(getClass());

    private boolean hasBeenInitialized = false;


    static
    {
        if (!Subsystems.hasInitAllBeenCalled())
        {
            final String msg = "Subsystems.initAllSubsystems() must be called before accessing/using any subsystems. It should typically be called in Robot.robotInit()";
            LOG.error("");
            LOG.error("FATAL ERROR: {}", msg);
            LOG.error("");
            // We want to let the logging framework flush since throwing this will result in an ExceptionInInitializerError
            // which typically dumps immediately to the console. This can cause the log message and console output to get
            // intermixed causing confusing output.
            try {TimeUnit.MILLISECONDS.sleep(700);} catch (InterruptedException ignore) {}
            throw new IllegalStateException(msg);
        }
    }

    @API
    protected Abstract3838Subsystem(String name)
    {
        super(name);
    }


    @API
    protected Abstract3838Subsystem()
    {
        super();
    }


    protected abstract void initDefaultCommandImpl() throws Exception;


    @SuppressWarnings("Duplicates")
    @Override
    public final void initializeTheSubsystem()
    {
        if (!hasBeenInitialized)
        {
            hasBeenInitialized = true;
            final Class<? extends Abstract3838Subsystem> clazz = getClass();
            enabled = Subsystems.checkIfEnabled(clazz);
            logger.debug("In Abstract3838Subsystem.initializeTheSubsystem(), when checking if '{}' is enabled, Subsystems.checkIfEnabled(clazz) returned {}", clazz.getSimpleName(), enabled);
            if (enabled)
            {
                try
                {
                    LOG.info("Initializing '{}' by calling its initSubsystem() method", getName());
                    initSubsystem();
                }
                catch (Exception e)
                {
                    LOG.warn("An exception occurred when initializing {}. It will be DISABLED. Cause summary: {}", getName(), e.toString(), e);
                    disableSubsystem();
                }
            }
            else
            {
                LOG.warn("{} is set as disabled, and will not be fully initialized.", getName());
            }
        }
    }


    @Override
    protected void initDefaultCommand()
    {
        if (isEnabled())
        {
            try
            {
                initDefaultCommandImpl();
            }
            catch (Exception e)
            {
                logger.warn("Unable to initialize default command for {}. Cause Summary: {}", getName(), e.toString(), e);
            }
        }

    }


    public boolean isEnabled() { return enabled; }


    @Override
    public void disableSubsystem()
    {
        LOG.warn("{} is being disabled.", getName());
        enabled = false;
    }


    /**
     * Called in the constructor if the subsystem is enabled.
     * If a subsystem does not have any initialization work to do during construction, simple create
     * a no op (no operation) implementation. That is, an empty method. implementations can throw
     * Exceptions. The caller in the super class, {@link Abstract3838Subsystem} will catch the
     * Exception and then set the subsystem to disabled.
     *
     * @throws Exception if any issues occur during initialization. The super constructor
     *                   in {@link Abstract3838Subsystem} will catch the Exception and then set the subsystem to
     *                   disabled.
     */
    protected abstract void initSubsystem() throws Exception;


    @Nonnull
    public Logger getLogger()
    {
        return (logger != null) ? logger : LoggerFactory.getLogger(Abstract3838Subsystem.class);
    }
}
