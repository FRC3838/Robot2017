package frc.team3838.core.subsystems;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import edu.wpi.cscore.CvSink;
import edu.wpi.cscore.CvSource;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;
import frc.team3838.core.meta.API;
import frc.team3838.core.meta.DoesNotThrowExceptions;


// *******************************************************
//   On SmartDashboard, use "CameraServer Stream Viewer"
// *******************************************************

public abstract class Abstract3838UsbCameraSubsystem extends Abstract3838Subsystem
{
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Abstract3838UsbCameraSubsystem.class);

    protected Map<Integer, UsbCamera> cameras = new HashMap<>(5);
    protected Map<String, Thread> cameraThreads = new HashMap<>(5);

    public enum CameraOperationMode { @API Simple, @API Advanced}


    protected Abstract3838UsbCameraSubsystem()
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem();
        super();
    }


    protected abstract int getDefaultCameraDeviceNumber();

    protected abstract CameraOperationMode getCameraImplMode();


    @API
    protected String createCameraName(int cameraDeviceNumber)
    {
        // return "cam" + cameraDeviceNumber;
        return "USB Camera " + cameraDeviceNumber;
    }

    @Override
    protected void initSubsystem() throws Exception
    {
        activateCamera(getDefaultCameraDeviceNumber());
    }
    
    protected void activateCamera(int cameraDeviceNumber) throws Exception
    {
        activateCamera(getOrCreateUsbCamera(cameraDeviceNumber));
    }
    
    protected void activateCamera(final UsbCamera camera) throws Exception
    {
        if (camera == null)
        {
            return;
        }
        
        switch (getCameraImplMode())
        {
            case Simple:
                activateCameraInSimpleMode(camera);
                break;
            case Advanced:
                activateCameraInAdvancedMode(camera);
                break;
            default:
                throw new IllegalStateException("Unsupported CameraOperationMode of: " + getCameraImplMode());
        }
    }

    @DoesNotThrowExceptions
    protected void deactivateCamera(int cameraDeviceNumber)
    {
        deactivateCamera(getOrCreateUsbCamera(cameraDeviceNumber));
    }
    
    @DoesNotThrowExceptions
    protected void deactivateCamera(final UsbCamera camera)
    {
        if (camera == null)
        {
            return;
        }
        
        try
        {
            final Thread cameraThread = cameraThreads.get(camera.getName());
            if (cameraThread != null)
            {
                cameraThread.interrupt();
            }
        }
        catch (Exception e)
        {
            logger.warn("An exception occurred when interrupting camera background thread prior to switching cameras. Cause Summary: {}", e.toString(), e);
        }

        try
        {
            CameraServer.getInstance().removeCamera(camera.getName());
        }
        catch (Exception e)
        {
            logger.warn("An exception occurred when removing previous camera from CameraServer prior to switching cameras. Cause Summary: {}", e.toString(), e);
        }
    }

    
    protected void activateCameraInSimpleMode(int cameraDeviceNumber)
    {
        activateCameraInSimpleMode(getOrCreateUsbCamera(cameraDeviceNumber));
    }


    protected void activateCameraInSimpleMode(final UsbCamera camera)
    {
        if (!cameras.containsKey(camera.getHandle()))
        {
            cameras.put(camera.getHandle(), camera);
        }
        CameraServer.getInstance().startAutomaticCapture(camera);
    }

    protected void activateCameraInAdvancedMode(int cameraDeviceNumber) throws Exception
    {
        activateCameraInAdvancedMode(getOrCreateUsbCamera(cameraDeviceNumber));
    }


    protected void activateCameraInAdvancedMode(final UsbCamera camera) throws Exception
    {
        if (!cameras.containsKey(camera.getHandle()))
        {
            cameras.put(camera.getHandle(), camera);
        }
        
        // http://wpilib.screenstepslive.com/s/4485/m/24194/l/669166-using-the-camera-server-on-the-roborio-2017

        Thread cameraThread = new Thread(() ->
                                  {
                                      try
                                      {
                                          CameraServer.getInstance().startAutomaticCapture(camera);
                                          final int width = getResolutionWidthSetting();
                                          final int height = getResolutionHeightSetting();

                                          camera.setResolution(width, height);
                                          camera.setFPS(getFpsSetting());

                                          CvSink cvSink = CameraServer.getInstance().getVideo();
                                          CvSource outputStream = CameraServer.getInstance().putVideo("Blur", width, height);

                                          Mat source = new Mat();
                                          Mat output = new Mat();

                                          while (!Thread.interrupted())
                                          {
                                              cvSink.grabFrame(source);
                                              Imgproc.cvtColor(source, output, Imgproc.COLOR_BGR2GRAY);
                                              outputStream.putFrame(output);
                                          }
                                      }
                                      catch (Exception e)
                                      {
                                          logger.warn("An exception occurred in camera background thread. Cause Summary: {}", e.toString(), e);
                                      }
                                  });
        cameraThreads.put(camera.getName(), cameraThread);
        cameraThread.start();
    }


    @Nonnull
    protected UsbCamera getOrCreateUsbCamera(int cameraDeviceNumber)
    {
        return cameras.computeIfAbsent(cameraDeviceNumber, integer -> new UsbCamera(createCameraName(cameraDeviceNumber), cameraDeviceNumber));
    }


    @API
    protected void switchCameras(int oldCameraDeviceNumber, int newCameraDeviceNumber)
    {
        try
        {
            deactivateCamera(getOrCreateUsbCamera(oldCameraDeviceNumber));
            activateCamera(newCameraDeviceNumber);
        }
        catch (Exception e)
        {
            logger.error("Cannot switch camera. An exception occurred when initializing new camera. Cause Summary: {}", e.toString(), e);
        }
    }
    
   
    /*
            ****************************************************************************** 
            ** For bandwidth usage at various Resolutions and Frame rates, see the      **
            ** "Bandwidth consumption for different camera resolutions" section at      **
            ** http://wpilib.screenstepslive.com/s/4485/m/24193/l/291972-fms-whitepaper **
            ****************************************************************************** 
            
            <<<Each team VLAN is limited to 7 megabits/second (Mbps).>>>
            
            This data was captured using an Axis 1011, but the data is also applicable to the Axis 206.
            
         ** Stream      Image       Frame   |--- Peak Data Rate in Mbps  ---| **
         ** Type        Size        Rate    compression = 0  compression = 30 **
            MJPEG       640x480     30          34.0 Mbps         14.0 Mbps
            MJPEG       640x480     24          25.5 Mbps         11.1 Mbps
            MJPEG       640x480     15          15.5 Mbps          6.1 Mbps
            MJPEG       640x480     10          11.0 Mbps          4.3 Mbps
            
            MJPEG       320x240     30           8.0 Mbps          3.7 Mbps       <== Recommended settings with compression of 30
            MJPEG       320x240     24           7.5 Mbps          2.9 Mbps
            MJPEG       320x240     15           4.2 Mbps          2.1 Mbps
            MJPEG       320x240     10           2.8 Mbps          1.2 Mbps
            
            MJPEG       160x120     30           2.9 Mbps          1.0 Mbps
            MJPEG       160x120     24           2.0 Mbps          0.9 Mbps
            MJPEG       160x120     15           1.1 Mbps          0.6 Mbps
            MJPEG       160x120     10           0.8 Mbps          0.4 Mbps
     */


    protected int getResolutionWidthSetting() { return 320; }


    protected int getResolutionHeightSetting() { return 240; }


    protected int getFpsSetting() { return 30; }


    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        // Initialize the default command, if any, here.
        // A default command runs without any driver action
        // Default Commands are not common for most subsystems
    }

}

