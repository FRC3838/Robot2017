package frc.team3838.core.subsystems;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableMap;

import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838PIDDriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.I3838DriveTrainSubsystem;
import frc.team3838.core.utils.ReflectionUtils3838;



public final class Subsystems
{
    private static final Logger LOG = LoggerFactory.getLogger(Subsystems.class);
    public static final String GET_INSTANCE_METHOD_NAME = "getInstance";

    private static SortedSet<Class<? extends Subsystem>> invalidSubsystems;

    private static final Map<Class<? extends I3838Subsystem>, I3838Subsystem> instances = new HashMap<>();

    private static boolean hasInitAllBeenCalled = false;

    @Nullable
    private static I3838DriveTrainSubsystem driveTrainSubsystem;

    @Nullable
    private static ImmutableMap<Class<?extends I3838Subsystem>,Boolean> enabledSubsystemsMap;

    /**
     * Validates and initializes all Subsystems. This method must be called prior to accessing any
     * of the subsystems.  It is typically called in Robot.robotInt(). This method can be called multiple
     * times without any ill effects.
     * Returns a (possibly, and hopefully, empty) Set of Subsystems that either do not extend the
     * Abstract3838Subsystem and/or do not follow team 3838 Subsystem coding standards.
     *
     * @param enabledSubsystemsMap a map of subsystem classes to a boolean to indicate is the subsystem is enabled or not
     *
     * @return a set of invalid Subsystems; i.e. Subsystems that do not follow the Team 3838 coding policy
     */
    @SuppressWarnings("UnusedReturnValue")
    public static SortedSet<Class<? extends Subsystem>> initAllSubsystems(@Nonnull ImmutableMap<Class<? extends I3838Subsystem>, Boolean> enabledSubsystemsMap)
    {
        if (hasInitAllBeenCalled)
        {
            //TODO: Improve this so an exception is only thrown if the received enabledSubsystemsMap is different from the previous
            final String msg = "Subsystems.initAllSubsystems() has already been called. It should oly be called once, typically in the Robot.robotInit() method.";
            LOG.error(msg);
            throw new IllegalStateException(msg);
        }
        hasInitAllBeenCalled = true;
        Subsystems.enabledSubsystemsMap = enabledSubsystemsMap;
        if (invalidSubsystems == null)
        {
            doInitAll();
        }
        return invalidSubsystems;
    }


    public static boolean hasInitAllBeenCalled()
    {
        return hasInitAllBeenCalled;
    }


    private static void doInitAll()
    {
        invalidSubsystems = new TreeSet<>(Comparator.comparing(Class::getName));
        final Set<Class<? extends Subsystem>> subsystems = ReflectionUtils3838.findImplementationsFiltered(Subsystem.class, Abstract3838Subsystem.class);

        final SortedSet<String> initializedSubSystemNames = new TreeSet<>();
        final SortedSet<String> disabledSubSystemNames = new TreeSet<>();

        final List<Class<? extends I3838DriveTrainSubsystem>> foundDriveTrainSubsystems = new ArrayList<>();

        if (LOG.isInfoEnabled())
        {
            final SortedSet<String> checkingForValidityNames = new TreeSet<>();
            for (Class<? extends Subsystem> subsystem : subsystems) { checkingForValidityNames.add(subsystem.getSimpleName());}
            final SortedSet<String> toBeInitializedNames = new TreeSet<>();
            assert enabledSubsystemsMap != null;
            for (Class<? extends I3838Subsystem> subsystem : enabledSubsystemsMap.keySet()) { toBeInitializedNames.add(subsystem.getSimpleName());}
            LOG.info("================================================================================================");
            LOG.info("`SUBSYSTEM INITIALIZATION`");
            LOG.info("Checking if the following subsystems meet coding standards: {}", checkingForValidityNames);
            LOG.info("If valid, the following subsystems will be initialized:     {}", toBeInitializedNames);
            LOG.info("To add a subsystem to the list to be initialized, add it to the mapping passed into Subsystems.initAllSubsystems(). Usually this map is configured in the RobotMap class.");
            LOG.info("================================================================================================");
        }


        for (Class<? extends Subsystem> subsystemClass : subsystems)
        {
            boolean isValid = true;
            final String simpleName = subsystemClass.getSimpleName();
            LOG.trace("Checking subsystem {}", simpleName);

            // === Coding Standards compliance checks ===

            // 1) Does it extend the Team 3838 Subsystem (rather then just Subsystem directly(
            if (!Abstract3838Subsystem.class.isAssignableFrom(subsystemClass) && !Abstract3838PIDSubsystem.class.isAssignableFrom(subsystemClass))
            {
                isValid = false;
                LOG.error("The '{}' Subsystem class does not extend Abstract3838Subsystem or Abstract3838PIDSubsystem.", simpleName);
            }

            // 2) Does it have a public static getInstance() method
            try
            {
                final Method getInstanceMethod = subsystemClass.getDeclaredMethod(GET_INSTANCE_METHOD_NAME);
                final int modifiers = getInstanceMethod.getModifiers();
                if (!Modifier.isPublic(modifiers) || !Modifier.isStatic(modifiers))
                {
                    isValid = false;
                    LOG.error("The '{}' Subsystem class getInstance() method is not public and/or static. It should be: @NonNull public static {} getInstance()",
                              simpleName, simpleName);
                }
                final Class<?> returnType = getInstanceMethod.getReturnType();
                if (!returnType.equals(subsystemClass))
                {
                    isValid = false;
                    LOG.error(
                        "The '{}' Subsystem class getInstance() method is does not return the same type as the class definition. A possible copy and paste error? "
                        + "Expected Type: '{}'; actual type: {}",
                        simpleName,
                        subsystemClass.getName(),
                        returnType.getName());
                }

                final Nonnull nonnullAnno = getInstanceMethod.getAnnotation(Nonnull.class);
                if (nonnullAnno == null)
                {
                    isValid = false;
                    LOG.error(
                        "The '{}' Subsystem class getInstance() method is does not annotated with an @NonNull annotation. "
                        + "It should be: @NonNull public static {} getInstance()", simpleName, simpleName);
                }

            }
            catch (NoSuchMethodException e)
            {
                isValid = false;
                LOG.error("The '{}' Subsystem class does not have a public static getInstance() method. It should have the method: "
                          + "@NonNull public static {} getInstance()", simpleName, simpleName);
            }
            catch (Throwable e)
            {
                isValid = false;
                LOG.error("An exception occurred when validating that the '{}' Subsystem class has a public static getInstance() method. Cause Summary: {}", simpleName, e.toString(), e);
            }


            // 3) Are the constructors protected or private
            final Constructor<?>[] constructors = subsystemClass.getConstructors();
            for (Constructor<?> constructor : constructors)
            {
                if (Modifier.isPublic(constructor.getModifiers()))
                {
                    isValid = false;
                    LOG.error("The '{}' Subsystem class has a public constructor. All constructors should be private (pr protected) to ensure "
                              + "it is used as a Singleton via the public static getInstance() method. This could be the default public no-arg "
                              + "constructor; if so, just add: private {}() { }", simpleName, simpleName);
                    break;
                }
            }


            if (simpleName.toLowerCase().contains("DriveTrain".toLowerCase()))
            {
                if (!I3838DriveTrainSubsystem.class.isAssignableFrom(subsystemClass))
                {
                    isValid = false;
                    LOG.error("The '{}' Subsystem class contains the name DriveTrain, but does not extend the {} "
                              + "or {} class.",
                              simpleName,
                              Abstract3838DriveTrainSubsystem.class.getSimpleName(),
                              Abstract3838PIDDriveTrainSubsystem.class.getSimpleName());
                }
            }


            if (I3838DriveTrainSubsystem.class.isAssignableFrom(subsystemClass))
            {
                //noinspection unchecked
                foundDriveTrainSubsystems.add((Class<? extends I3838DriveTrainSubsystem>) subsystemClass);
            }


            if (!simpleName.endsWith("Subsystem"))
            {
                LOG.warn("The '{}' subsystem's name does not end with 'Subsystem'. Our naming convention is to have all Subsystems "
                         + "implementations' name to end in 'Subsystem'. Please rename this subsystem.", simpleName);
            }


            if (!isValid)
            {
                invalidSubsystems.add(subsystemClass);
            }
            else
            {
                //It's valid, so let's (try to) initialize it if requested...
                try
                {
                    //noinspection unchecked  - We checked above
                    final I3838Subsystem subsystem = getInstanceOf((Class<? extends I3838Subsystem>) subsystemClass);
                    if (subsystem != null)
                    {
                        initializedSubSystemNames.add(subsystem.getName());
                        if (!subsystem.isEnabled())
                        {
                            LOG.debug("{} is disabled. Adding to disabledSubSystemNames Set", subsystem.getName());
                            disabledSubSystemNames.add(subsystem.getName());
                        }
                    }
                }
                catch (Exception e)
                {
                    LOG.warn("An exception occurred when reflectively initializing the '{}' subsystem. Cause Summary: {}", e.toString(), subsystemClass);
                    invalidSubsystems.add(subsystemClass);
                }
            }
        }


        if (foundDriveTrainSubsystems.size() > 1)
        {
            LOG.error("More than 1 DriveTrain implementation was found. There should only be a single DriveTrain Subsystem. "
                      + "Found implementations: {}", foundDriveTrainSubsystems);
        }
        else if (foundDriveTrainSubsystems.isEmpty())
        {
            LOG.warn("No DriveTrain subsystem was found. There should be one subsystem that extends either "
                     + "{} or {} class.",
                     Abstract3838DriveTrainSubsystem.class.getSimpleName(),
                     Abstract3838PIDDriveTrainSubsystem.class.getSimpleName());
        }
        else
        {
            driveTrainSubsystem = getInstanceOf(foundDriveTrainSubsystems.get(0));
        }


        if ((invalidSubsystems.isEmpty()))
        {
            LOG.info("Successfully initialized subSystems: {}", initializedSubSystemNames);
            if (disabledSubSystemNames.isEmpty())
            {
                LOG.info("No subsystems are disabled");
            }
            else
            {
                LOG.warn("Disabled Subsystems:                 {}", disabledSubSystemNames);
            }
        }
        else
        {
            final SortedSet<String> simpleNames = new TreeSet<>();
            for (Class<? extends Subsystem> subsystem : invalidSubsystems) { simpleNames.add(subsystem.getSimpleName());}
            final String phrase = invalidSubsystems.size() == 1 ? "subsystem does" : "subsystems do";
            LOG.error("*** THERE ARE SIGNIFICANT CONFIGURATION PROBLEMS! ***\n"
                      + "ERROR: ****************************************************************************\n"
                      + "ERROR: *      *** THERE ARE SIGNIFICANT CONFIGURATION PROBLEMS! ***                \n"
                      + "ERROR: * {} {} not follow the Team 3838 coding standard for subsystems. \n" //Spacing is different for this line due to placeholders
                      + "ERROR: * See previous log messages for details on the issues. Please               \n"
                      + "ERROR: * see the README.md document in the subsystems package for our              \n"
                      + "ERROR: * subsystems coding policy. In summary, all Subsystems must:                \n"
                      + "ERROR: * 1) extend either Abstract3838Subsystem or Abstract3838PIDSubsystem;       \n"
                      + "ERROR: * 2) be Singletons such that they have:                                     \n"
                      + "ERROR: *    a) a private constructor (or constructors)                             \n"
                      + "ERROR: *    b) a public static getInstance() method                                \n"
                      + "ERROR: * Subsystems that have issues:                                              \n"
                      + "ERROR: * {}\n"
                      + "ERROR: ****************************************************************************\n",
                      invalidSubsystems.size(), phrase, simpleNames);
        }
    }


    @Nullable
    public static <T extends I3838Subsystem> T getInstanceOf(Class<T> type)
    {
        if (enabledSubsystemsMap != null && enabledSubsystemsMap.containsKey(type))
        {
            //noinspection unchecked
            return (T) instances.computeIfAbsent(type, aClass -> createAnInstance(type));
        }
        else
        {
            return null;
        }
    }


    private static <T extends I3838Subsystem> T createAnInstance(Class<T> type)
    {
        try
        {
            final Method getInstanceMethod = type.getDeclaredMethod(GET_INSTANCE_METHOD_NAME);
            getInstanceMethod.setAccessible(true);
            @SuppressWarnings("unchecked")
            final T instance = (T) getInstanceMethod.invoke(null);
            instance.initializeTheSubsystem();
            return instance;
        }
        catch (Exception e)
        {
            String msg = "Could not reflectively get an instance of the '" + type.getName() + "' Subsystem. "
                         + "Cause Summary: " + e.toString();
            LOG.error(msg, e);
            throw new IllegalStateException(msg, e);
        }
    }


    public static boolean checkIfEnabledInRobotMap(@Nonnull Class<? extends I3838Subsystem> subsystemClass)
    {
        if (enabledSubsystemsMap != null)
        {
            final Boolean enabledSetting = enabledSubsystemsMap.get(subsystemClass);
            if (enabledSetting == null)
            {
                return false;
            }
            else
            {
                return enabledSetting;
            }
        }
        else
        {
            throw new IllegalStateException("initAllSubsystems() must be called prior to calling checkIfEnabled()");
        }
    }

    public static boolean checkIfEnabled(@Nonnull Class<? extends I3838Subsystem> subsystemClass)
    {
        final I3838Subsystem subsystemInstance = instances.get(subsystemClass);
        if (subsystemInstance != null)
        {
            return subsystemInstance.isEnabled();
        }

        return checkIfEnabledInRobotMap(subsystemClass);
    }


    @SuppressWarnings("unused")
    @Nullable
    public static I3838DriveTrainSubsystem getDriveTrainSubsystem() { return driveTrainSubsystem; }
}
