package frc.team3838.core.subsystems.drive;

import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.RobotDrive;
import frc.team3838.core.meta.DoesNotThrowExceptions;
import frc.team3838.core.subsystems.Abstract3838PIDSubsystem;



public abstract class Abstract3838PIDDriveTrainSubsystem extends Abstract3838PIDSubsystem implements I3838DriveTrainSubsystem
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Nullable
    protected RobotDrive robotDrive;


    @SuppressWarnings("unused")
    protected Abstract3838PIDDriveTrainSubsystem(String name, double p, double i, double d)
    {
        super(name, p, i, d);
    }


    @SuppressWarnings("unused")
    protected Abstract3838PIDDriveTrainSubsystem(String name, double p, double i, double d, double f)
    {
        super(name, p, i, d, f);
    }


    @SuppressWarnings("unused")
    protected Abstract3838PIDDriveTrainSubsystem(String name, double p, double i, double d, double f, double period)
    {
        super(name, p, i, d, f, period);
    }


    @SuppressWarnings("unused")
    protected Abstract3838PIDDriveTrainSubsystem(double p, double i, double d)
    {
        super(p, i, d);
    }


    @SuppressWarnings("unused")
    protected Abstract3838PIDDriveTrainSubsystem(double p, double i, double d, double period, double f)
    {
        super(p, i, d, period, f);
    }


    @SuppressWarnings("unused")
    protected Abstract3838PIDDriveTrainSubsystem(double p, double i, double d, double period)
    {
        super(p, i, d, period);
    }


    @SuppressWarnings("unused")
    @DoesNotThrowExceptions
    public void setExpiration(double timeout)
    {
        if (robotDrive != null)
        {
            try
            {
                robotDrive.setExpiration(timeout);
            }
            catch (Exception e)
            {
                logger.warn("An exception was thrown when robotDrive.setExpiration() was called. Cause summary: {}", e.toString(), e);
            }
        }
    }


    @Override
    @DoesNotThrowExceptions
    public void disableMotorSafetyForAutonomousMode()
    {
        if (robotDrive != null)
        {
            try
            {
                robotDrive.setSafetyEnabled(false);
            }
            catch (Exception e)
            {
                logger.info("An exception was thrown when robotDrive.setSafetyEnabled(false) was called. Cause summary: {}", e.toString(), e);
            }
        }
    }


    @Override
    @DoesNotThrowExceptions
    public void enableMotorSafetyPostAutonomousMode()
    {
        // For enabling the motor safety, we don't check if the system is enabled. We want
        // to err on the side of caution in case the enabled boolean is out of sync, and
        // always enable the safeties when asked.
        // Runaway robots are a bad thing :)
        if (robotDrive != null)
        {
            try
            {
                robotDrive.setSafetyEnabled(true);
            }
            catch (Exception e)
            {
                logger.warn("An exception was thrown when robotDrive.setSafetyEnabled(true) was called. Cause summary: {}", e.toString(), e);
            }
        }
    }


    @Override
    @DoesNotThrowExceptions
    public void stop()
    {
        // For stopping, we don't check if the system is enabled. We want to err on the side
        // of caution in case the enabled boolean is out of sync and thus stop the robot..
        // Runaway robots are a bad thing :)
        if (robotDrive != null)
        {
            try
            {
                robotDrive.stopMotor();
            }
            catch (Exception e)
            {
                logger.warn("An exception was thrown when robotDrive.stopMotor() was called. Cause summary: {}", e.toString(), e);
            }
        }
    }

}
