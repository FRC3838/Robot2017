package frc.team3838.core.subsystems.drive;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableSet;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.MotorSafetyHelper;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.controls.AxisPair;
import frc.team3838.core.hardware.DisabledRobotDrive;
import frc.team3838.core.hardware.EncoderConfig;
import frc.team3838.core.hardware.EnhancedEncoder;
import frc.team3838.core.hardware.FeedableRobotDrive;
import frc.team3838.core.meta.API;
import frc.team3838.core.meta.DoesNotThrowExceptions;
import frc.team3838.core.meta.Max;
import frc.team3838.core.meta.Min;
import frc.team3838.core.subsystems.Abstract3838Subsystem;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.utils.MathUtils;



@SuppressWarnings({"UnnecessaryToStringCall", "BooleanMethodNameMustStartWithQuestion", "CallToSimpleGetterFromWithinClass"})
@API
public abstract class Abstract3838DriveTrainSubsystem extends Abstract3838Subsystem implements I3838DriveTrainSubsystem
{
    private static final Logger logger = LoggerFactory.getLogger(Abstract3838DriveTrainSubsystem.class);

    @Nullable
    protected EnhancedEncoder leftEncoder;
    @Nullable
    protected EnhancedEncoder rightEncoder;

    private double joystickDriftZoneThreshold = 0.005;

    /**
     * Flag to indicate if the input values should be 'curved' in order to
     * decrease the sensitivity at low speeds. It does this by raising the
     * input value to power specified by the {@link #curveFactor}. For example,
     * if set to true, and the {@code curveFactor} is 2, the operator input
     * value (typically from the joystick) is squared before sending the value
     * to the motor. This changes the response from a linear response to a
     * curved response.
     */
    private boolean shouldCurveInput = false;

    /**
     * The exponent used when applying a sensitivity curve to the
     * an value before sending to the motor controller.
     */
    private int curveFactor = 2;


    @Nonnull
    private final SpeedController leftSpeedController;
    @Nonnull
    private final SpeedController rightSpeedController;
    @Nonnull
    private final RobotDrive robotDrive;

    private long intermittentLoggingLastTime = 0;
    private long speedSettingLoggingLastTime = 0;

    @Nullable
    private MotorSafetyHelper robotDriveMotorSafetyHelper;

    @Nonnull
    private DriveTrainControlConfig driveTrainControlConfig;


    @Nullable
    public EnhancedEncoder getLeftEncoder()
    {
        return leftEncoder;
    }


    @Nullable
    public EnhancedEncoder getRightEncoder()
    {
        return rightEncoder;
    }


    public double getAverageDistanceInInches()
    {
        double total = 0;
        int count = 0;

        if (leftEncoder != null)
        {
            total += leftEncoder.getDistanceInInches();
            count++;
        }

        if (rightEncoder != null)
        {
            total += rightEncoder.getDistanceInInches();
            count++;
        }
        return count == 0 ? 0 : total / count;
    }


    @API
    public double getLeftDistanceInInches()
    {
        return leftEncoder == null ? 0 : leftEncoder.getDistanceInInches();
    }


    @API
    public double getRightDistanceInInches()
    {
        return rightEncoder == null ? 0 : rightEncoder.getDistanceInInches();
    }


    @API
    public enum RobotDirection
    {
        @API Forward, @API Reverse
    }


    @API
    protected Abstract3838DriveTrainSubsystem(@Nonnull SpeedController leftSpeedController,
                                              @Nonnull SpeedController rightSpeedController,
                                              @Nonnull DriveTrainControlConfig driveTrainControlConfig)
    {
        if (isEnabled())
        {
            logger.debug("In Abstract3838DriveTrainSubsystem - isEnabled returning true");
            Objects.requireNonNull(leftSpeedController, "'leftSpeedController' cannot be null");
            Objects.requireNonNull(rightSpeedController, "'rightSpeedController' cannot be null");
            Objects.requireNonNull(driveTrainControlConfig, "'driveTrainControlConfig' cannot be null");
            this.leftSpeedController = leftSpeedController;
            this.rightSpeedController = rightSpeedController;
            this.robotDrive = new RobotDrive(leftSpeedController, rightSpeedController);
            this.driveTrainControlConfig = driveTrainControlConfig;

            try
            {
                final Field safetyHelperField = robotDrive.getClass().getDeclaredField("m_safetyHelper");
                safetyHelperField.setAccessible(true);
                this.robotDriveMotorSafetyHelper = (MotorSafetyHelper) safetyHelperField.get(robotDrive);
                feedRobotDriveMotorSafety();
            }
            catch (Exception e)
            {
                logger.debug("Unable to initialize robotDriveMotorSafetyHelper due to an exception. robotDrive class type is '{}'. Cause summary: {}",
                             robotDrive.getClass(), e.toString(), e);
            }
        }
        else
        {
            logger.warn("In Abstract3838DriveTrainSubsystem constructor, isEnabled() returned false. Configuring DriveTrain in \"Disabled Mode\".");
            final DisabledRobotDrive disabledRobotDrive = new DisabledRobotDrive();
            this.robotDrive = disabledRobotDrive;
            this.leftSpeedController = disabledRobotDrive.getSpeedController();
            this.rightSpeedController = disabledRobotDrive.getSpeedController();
            this.driveTrainControlConfig = DriveTrainControlConfigImpl.getNoOpDriveTrainControlConfig();
        }
    }


    @API
    @Nonnull
    public SpeedController getLeftSpeedController() { return leftSpeedController; }


    @API
    @Nonnull
    public SpeedController getRightSpeedController() { return rightSpeedController; }


    @SuppressWarnings("unused")
    @DoesNotThrowExceptions
    public void setExpiration(double timeout)
    {
        try
        {
            robotDrive.setExpiration(timeout);
        }
        catch (Exception e)
        {
            logger.warn("An exception was thrown when robotDrive.setExpiration() was called. Cause summary: {}", e.toString(), e);
        }
    }


    @Override
    @DoesNotThrowExceptions
    public void disableMotorSafetyForAutonomousMode()
    {
        try
        {
            robotDrive.setSafetyEnabled(false);
        }
        catch (Exception e)
        {
            logger.info("An exception was thrown when robotDrive.setSafetyEnabled(false) was called. Cause summary: {}", e.toString(), e);
        }
    }


    @Override
    @DoesNotThrowExceptions
    public void enableMotorSafetyPostAutonomousMode()
    {
        // For enabling the motor safety, we don't check if the system is enabled. We want
        // to err on the side of caution in case the enabled boolean is out of sync, and
        // always enable the safeties when asked.
        // Runaway robots are a bad thing :)
        try
        {
            //We feed it just prior to enabling to prevent a potential one time warning upon enabling.
            feedRobotDriveMotorSafety();
            robotDrive.setSafetyEnabled(true);
        }
        catch (Exception e)
        {
            logger.warn("An exception was thrown when robotDrive.setSafetyEnabled(true) was called. Cause summary: {}", e.toString(), e);
        }
    }


    public void feedRobotDriveMotorSafety()
    {
        try
        {
            if (robotDriveMotorSafetyHelper != null)
            {
                robotDriveMotorSafetyHelper.feed();
            }

            if (robotDrive instanceof FeedableRobotDrive)
            {
                ((FeedableRobotDrive) robotDrive).feedMotorSafety();
            }
        }
        catch (Exception e)
        {
            logger.debug("Unable to feed robot drive motor safety due to an exception. Cause summary: {}", e.toString(), e);
        }
    }


    @Override
    @DoesNotThrowExceptions
    public void stop()
    {
        // For stopping, we don't check if the system is enabled. We want to err on the side
        // of caution in case the enabled boolean is out of sync and thus stop the robot..
        // Runaway robots are a bad thing :)
        try
        {
            robotDrive.stopMotor();
        }
        catch (Exception e)
        {
            logger.warn("An exception was thrown when robotDrive.stopMotor() was called. Cause summary: {}", e.toString(), e);
        }
    }


    protected void initEncoders()
    {
        try
        {
            leftEncoder = createEncoder(getLeftEncoderConfig());
            rightEncoder = createEncoder(getRightEncoderConfig());
        }
        catch (Exception e)
        {
            logger.warn("Unable to initialize Encoders in DriveTrain. Cause Summary: {}", e.toString(), e);
        }
    }


    /**
     * Gets the encoder configuration for left side encoder. If an implmenting class does not use encoders
     * (that is the current yearsRobot does not use encoders), implement this method to just return
     * null.
     *
     * @return the left encoder config
     */
    @Nullable
    protected abstract EncoderConfig getLeftEncoderConfig();

    /**
     * Gets the encoder configuration for right side encoder. If an implmenting class does not use encoders
     * (that is the current yearsRobot does not use encoders), implement this method to just return
     * null.
     *
     * @return the right encoder config
     */
    @Nullable
    protected abstract EncoderConfig getRightEncoderConfig();


    @Nullable
    protected static EnhancedEncoder createEncoder(@Nullable EncoderConfig encoderConfig)
    {

        return encoderConfig == null ? null : createEncoder(encoderConfig.getChannelA(),
                                                            encoderConfig.getChannelB(),
                                                            encoderConfig.getDistancePerPulse(),
                                                            encoderConfig.isInverted());
    }


    /**
     * @param encoderChA       Digital Input Channel A
     * @param encoderChB       Digital Input Channel B
     * @param distancePerPulse inches per pulse
     * @param inverse          if the reading should be inverted
     *
     * @return an encoder
     */
    protected static EnhancedEncoder createEncoder(int encoderChA, int encoderChB, double distancePerPulse, boolean inverse)
    {
        final DigitalInput leftChA = new DigitalInput(encoderChA);
        final DigitalInput leftChB = new DigitalInput(encoderChB);

        // We are using AndyMark SKU am-3132 encoders, which are US Digital E4T-360-250    http://www.andymark.com/E4T-OEM-Miniature-Optical-Encoder-Kit-p/am-3132.htm
        //  Cycles per Revolution: 360
        //  Pulses per Revolution:  1440


        // See http://wpilib.screenstepslive.com/s/4485/m/13809/l/599717-encoders-measuring-rotation-of-a-wheel-or-other-shaft
        final EnhancedEncoder encoder = new EnhancedEncoder(leftChA, leftChB, false, Encoder.EncodingType.k4X);

        encoder.setMaxPeriod(.1);
        encoder.setMinRate(10);
        encoder.setDistancePerPulse(distancePerPulse); // inches per pulse
        encoder.setReverseDirection(inverse);
        encoder.setSamplesToAverage(8);

        encoder.reset();
        return encoder;

        /*

        The following parameters of the encoder class may be set through the code:

        Max Period -          The maximum period (in seconds) where the device is still considered moving. This value is used to determine the
                              state of the getStopped() method and effect the output of the getPeriod() and getRate() methods. This is the time
                              between pulses on an individual channel (scale factor is accounted for). It is recommended to use the Min Rate
                              parameter instead as it accounts for the distance per pulse, allowing you to set the rate in engineering units.

        Min Rate -            Sets the minimum rate before the device is considered stopped. This compensates for both scale factor and
                              distance per pulse and therefore should be entered in engineering units (RPM, RPS, Degrees/sec, In/s, etc)

        Distance Per Pulse -  Sets the scale factor between pulses and distance. The library already accounts for the decoding scale factor
                              (1x, 2x, 4x) separately so this value should be set exclusively based on the encoder's Pulses per Revolution
                              and any gearing following the encoder.

        Reverse Direction -   Sets the direction the encoder counts, used to flip the direction if the encoder mounting makes the default
                              counting direction unintuitive.

        Samples to Average -  Sets the number of samples to average when determining the period. Averaging may be desired to account for
                              mechanical imperfections (such as unevenly spaced reflectors when using a reflective sensor as an encoder)
                              or as oversampling to increase resolution. Valid values are 1 to 127 samples.



        The following values can be retrieved from the encoder:

        Count - The current count. May be reset by calling reset().
        Raw Count - The count without compensation for decoding scale factor.
        Distance - The current distance reading from the counter. This is the count multiplied by the Distance Per Count scale factor.
        Period - The current period of the counter in seconds. If the counter is stopped this value may return 0. This is deprecated, it is recommended to use rate instead.
        Rate - The current rate of the counter in units/sec. It is calculated using the DistancePerPulse divided by the period. If the counter is stopped this value may return Inf or NaN, depending on language.
        Direction - The direction of the last value change (true for Up, false for Down)
        Stopped - If the counter is currently stopped (period has exceeded Max Period)

         */

    }


    /**
     * Method to tell the DriveTrain to drive using the configured Joystick and the
     * configured control method. This method is primarily meant to be used by commands.
     */
    public void drive()
    {
        //noinspection ConstantConditions
        if (driveTrainControlConfig == null || driveTrainControlConfig.getAxisPair() == null)
        {
            if (System.currentTimeMillis() > (intermittentLoggingLastTime + 10_000))
            {
                logger.error("axisPair field is null. Cannot drive the robot.");
                intermittentLoggingLastTime = System.currentTimeMillis();
            }
        }
        else
        {
            final DriveControlMode driveControlMode = driveTrainControlConfig.getDriveControlMode();
            final AxisPair axisPair = driveTrainControlConfig.getAxisPair();
            if (logger.isDebugEnabled() && System.currentTimeMillis() > (intermittentLoggingLastTime + 10_000))
            {
                logger.debug("drive() controlMode = {}", driveControlMode);
                intermittentLoggingLastTime = System.currentTimeMillis();
            }


            switch (driveControlMode)
            {
                case Arcade:
                    driveViaArcadeControl(axisPair);
                    break;
                case Tank:
                    driveViaTankControl(axisPair);
                    break;
                default:
                    logger.error("Unhandled control mode of {}", driveControlMode.name());
                    throw new IllegalStateException("Unhandled control mode of " + driveControlMode.name());
            }

            if (shouldWriteEncoderValuesToSmartDashboard())
            {
                writeEncoderValuesToSmartDash();
            }
        }
    }


    @API
    @DoesNotThrowExceptions
    public void driveViaArcadeControl(@Nonnull GenericHID joystick)
    {
        driveViaArcadeControlFromHidInput(joystick.getX(), joystick.getY());
    }


    @API
    @DoesNotThrowExceptions
    public void driveViaArcadeControl(@Nonnull AxisPair axisPair)
    {
        driveViaArcadeControlFromHidInput(axisPair.getXorLeft(), axisPair.getYorRight());
    }


    @API
    @DoesNotThrowExceptions
    public void driveViaTankControl(@Nonnull GenericHID leftJoystick, GenericHID rightJoystick)
    {
        driveViaTankControlFromHidInput(leftJoystick.getY(), rightJoystick.getY());
    }


    @API
    @DoesNotThrowExceptions
    public void driveViaTankControl(@Nonnull AxisPair axisPair)
    {
        driveViaTankControlFromHidInput(axisPair.getXorLeft(), axisPair.getYorRight());
    }


    @DoesNotThrowExceptions
    public void driveViaArcadeControlFromHidInput(final double x, final double y)
    {
        try
        {
            if (isEnabled())
            {
                AdjustedAxisPairReading adjusted = new AdjustedAxisPairReading(x, y);
                final double adjustedX = adjusted.xOrLeft;
                final double adjustedY = adjusted.yOrRight;

                if (getDriveTrainControlConfig().getReverseModeTrigger() != null && getDriveTrainControlConfig().getReverseModeTrigger().get())
                {
                    //Activate reverse mode while trigger is held down
                    driveRobotViaArcadeControlRaw(curveAdjust(adjustedX), curveAdjust(-adjustedY));
                }
                else
                {
                    driveRobotViaArcadeControlRaw(curveAdjust(adjustedX), curveAdjust(adjustedY));
                }

            }
        }
        catch (Exception e)
        {
            stop();
            logger.error("An exception occurred in {}.driveViaArcadeControlFromHidInput(). Cause Summary: {}", getClass(), e.toString(), e);
        }
    }


    @SuppressWarnings("MethodMayBeStatic")
    protected double getFineControlMaxAbsoluteSpeed()
    {
        return .5;
    }


    /**
     * Drives the robot using the provided values, <strong>without</strong> any adjustments such as applying a
     * sensitivity curve, checking for joystick drift, etc. <strong>This method id primarily
     * meant to be called by autonomous processes.</strong>
     *
     * @param rotateValueX The value to use for the rotate right/left (represented by the X axis on a JoyStick in arcade mode)
     * @param moveValueY   The value to use for forwards/backwards movement (represented by the Y axis on a JoyStick in arcade mode)
     */
    @DoesNotThrowExceptions
    public void driveRobotViaArcadeControlRaw(double rotateValueX, double moveValueY)
    {
        try
        {
            if (isEnabled())
            {
                // The parameters in the robotDrive.arcadeDrive() method are labeled backwards.
                // We need to send the rotateValue as first arg and the move/speed value as the second arg
                robotDrive.arcadeDrive(rotateValueX, moveValueY, false);
            }
        }
        catch (Exception e)
        {
            stop();
            logger.error("An exception occurred in {}.driveRobotViaArcadeControlRaw(). Cause Summary: {}", getClass(), e.toString(), e);
        }
    }


    @DoesNotThrowExceptions
    public void driveViaTankControlFromHidInput(final double leftValue, final double rightValue)
    {
        try
        {
            if (isEnabled())
            {
                AdjustedAxisPairReading adjusted = new AdjustedAxisPairReading(leftValue, rightValue);
                final double adjustedLeft = adjusted.xOrLeft;
                final double adjustedRight = adjusted.yOrRight;

                if ((getDriveTrainControlConfig().getReverseModeTrigger() != null) && getDriveTrainControlConfig().getReverseModeTrigger().get())
                {
                    //Activate reverse mode while trigger is held down
                    driveRobotViaTankControlRaw(curveAdjust(-adjustedLeft), curveAdjust(-adjustedRight));
                }
                else
                {
                    driveRobotViaTankControlRaw(curveAdjust(adjustedLeft), curveAdjust(adjustedRight));
                }
            }
        }
        catch (Exception e)
        {
            stop();
            logger.error("An exception occurred in {}.driveViaTankControlFromHidInput(). Cause Summary: {}", getClass(), e.toString(), e);
        }
    }


    private class AdjustedAxisPairReading
    {
        private final double xOrLeft;
        private final double yOrRight;


        public AdjustedAxisPairReading(double xOrLeft, double yOrRight)
        {
            final double xIn = xOrLeft;
            final double yIn = yOrRight;
            if (Math.abs(xOrLeft) < joystickDriftZoneThreshold)
            {
                xOrLeft = 0;
            }

            if (Math.abs(yOrRight) < joystickDriftZoneThreshold)
            {
                yOrRight = 0;
            }


            if (getDriveTrainControlConfig().getFineControlTrigger() != null)
            {
                SmartDashboard.putBoolean("Fine Control", getDriveTrainControlConfig().getFineControlTrigger().get());
                if (getDriveTrainControlConfig().getFineControlTrigger().get())
                {
                    final boolean wasYNegative = MathUtils.isNegative(yOrRight);
                    yOrRight = MathUtils.scaleRange(Math.abs(yOrRight), 0, 1, 0, getFineControlMaxAbsoluteSpeed());
                    if (wasYNegative) { yOrRight = -yOrRight;}

                    final boolean wasXNegative = MathUtils.isNegative(xOrLeft);
                    xOrLeft = MathUtils.scaleRange(Math.abs(xOrLeft), 0, 1, 0, getFineControlMaxAbsoluteSpeed());
                    if (wasXNegative) { xOrLeft = -xOrLeft;}
                }
            }

            if (logger.isDebugEnabled() && System.currentTimeMillis() > (speedSettingLoggingLastTime + 2_000))
            {
                logger.debug("{} Mode  xOrLeft = {}, yOrRight = {}    xIn was {}, yIn was {}; Fine Control Max = {}  ",
                             getDriveTrainControlConfig().getDriveControlMode().name(),
                             xOrLeft,
                             yOrRight,
                             xIn,
                             yIn, getFineControlMaxAbsoluteSpeed());
                speedSettingLoggingLastTime = System.currentTimeMillis();
            }

            this.xOrLeft = xOrLeft;
            this.yOrRight = yOrRight;
        }
    }


    @DoesNotThrowExceptions
    public void driveRobotViaTankControlRaw(double leftValue, double rightValue)
    {
        try
        {
            if (isEnabled())
            {
                // For some reason, we need to invert the left side
                robotDrive.tankDrive(-leftValue, rightValue, false);
            }
        }
        catch (Exception e)
        {
            stop();
            logger.error("An exception occurred in {}.driveRobotViaTankControlRaw(). Cause Summary: {}", getClass(), e.toString(), e);
        }
    }


    public Abstract3838DriveTrainSubsystem getDriveTrainSubsystem() { return this; }


    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        setDefaultCommand(new Abstract3838Command()
        {
            @Nonnull
            @Override
            protected Set<I3838Subsystem> getRequiredSubsystems() { return ImmutableSet.of(getDriveTrainSubsystem()); }


            @Override
            protected void initializeImpl() throws Exception { }


            @Override
            protected void executeImpl() throws Exception
            {
                try
                {
                    if (isEnabled())
                    {
                        drive();
                    }
                    else
                    {
                        stop();
                        feedRobotDriveMotorSafety();
                    }
                }
                catch (Exception e)
                {
                    logger.error("An exception occurred in the default DriveCommand. Cause Summary: {}", e.toString(), e);
                }
            }


            @Override
            protected boolean isFinishedImpl() throws Exception { return false; }


            @Override
            protected void endImpl() throws Exception { }
        });
    }


    public void resetEncoders()
    {
        resetLeftEncoder();
        resetRightEncoder();
    }


    public void resetLeftEncoder()
    {
        if (leftEncoder != null) { leftEncoder.reset(); }
    }


    public void resetRightEncoder()
    {
        if (rightEncoder != null) { rightEncoder.reset();}
    }


    @SuppressWarnings("MethodMayBeStatic")
    @API
    protected boolean shouldWriteEncoderValuesToSmartDashboard()
    {
        return false;
    }


    /**
     * Gets the flag to indicate if the input values should be 'curved' in order to
     * decrease the sensitivity at low speeds. It does this by raising the
     * input value to power specified by the {@link #getCurveFactor()}. For example,
     * if set to true, and the {@code curveFactor} is 2, the operator input
     * value (typically from the joystick) is squared before sending the value
     * to the motor. This changes the response from a linear response to a
     * curved response. Default value is false.
     *
     * @return true if the input is set to be curved
     */
    @API
    public boolean getShouldCurveInput() { return shouldCurveInput; }


    /**
     * Sets the flag to indicate if the input values should be 'curved' in order to
     * decrease the sensitivity at low speeds. It does this by raising the
     * input value to power specified by the {@link #getCurveFactor()}. For example,
     * if set to true, and the {@code curveFactor} is 2, the operator input
     * value (typically from the joystick) is squared before sending the value
     * to the motor. This changes the response from a linear response to a
     * curved response. Default value is false.
     *
     * @param shouldCurveInput set to true if the input is set to be curved; default is false;
     */
    @API
    public void setShouldCurveInput(boolean shouldCurveInput) { this.shouldCurveInput = shouldCurveInput; }


    /**
     * Toggles the flag to indicate if the input values should be 'curved' in order to
     * decrease the sensitivity at low speeds. It does this by raising the
     * input value to power specified by the {@link #getCurveFactor()}. For example,
     * if set to true, and the {@code curveFactor} is 2, the operator input
     * value (typically from the joystick) is squared before sending the value
     * to the motor. This changes the response from a linear response to a
     * curved response.
     *
     * @return the new value
     */
    @API
    public boolean toggleShouldCurveInput()
    {
        this.shouldCurveInput = !this.shouldCurveInput;
        return this.shouldCurveInput;
    }


    /**
     * The exponent used when applying a sensitivity curve to the
     * an value before sending to the motor controller. Used only if the
     * {@link #getShouldCurveInput()} is set to true. Default value is 2,
     * indicating input values should be squared.
     */
    @API
    public int getCurveFactor() { return curveFactor; }


    /**
     * Sets the exponent used when applying a sensitivity curve to the
     * an value before sending to the motor controller. Used only if the
     * {@link #getShouldCurveInput()} is set to true. Default value is 2,
     * indicating input values should be squared.
     *
     * @param curveFactor the exponent to use when decreasing drive sensitivity
     *                    at low speeds. Default value is 2. Must be between 1 and
     *                    10 (inclusive) although values other than 2 or 3 are
     *                    typically not needed. A value of 1 would have no effect.
     */
    @API
    public void setCurveFactor(@Min(1) @Max(10) int curveFactor)
    {
        if ((curveFactor < 1) || (curveFactor > 10))
        {
            throw new IllegalArgumentException("The curve factor is out of allowed range. Should be between 1 and 10, inclusive. It was : " + curveFactor);
        }
        this.curveFactor = curveFactor;
    }

    @API
    @Nonnull
    public DriveTrainControlConfig getDriveTrainControlConfig()
    {
        return driveTrainControlConfig;
    }


    @API
    public void setDriveTrainControlConfig(@Nonnull DriveTrainControlConfig driveTrainControlConfig)
    {
        this.driveTrainControlConfig = driveTrainControlConfig;
    }


    /**
     * Gets the joystick drift zone threshold (absolute) value which is used to compensate for'joysticks that do not
     * reliably return to zero when 'released'. (This is usually because of weakened/stretched  springs in
     * the joystick.) Any values read from the joystick under this threshold
     * are zeroed out. The threshold applied to both the x and y axises in order to keep the joystick operation
     * as intuitive as possible.
     *
     * @return the threshold to use
     */
    @API
    public double getJoystickDriftZoneThreshold() { return joystickDriftZoneThreshold; }


    /**
     * Sets the joystick drift zone threshold (absolute) value which is used to compensate for'joysticks that do not
     * reliably return to zero when 'released'. (This is usually because of weakened/stretched  springs in
     * the joystick.) Any values read from the joystick under this threshold
     * are zeroed out. The threshold applied to both the x and y axises in order to keep the joystick operation
     * as intuitive as possible.
     *
     * @param joystickDriftZoneThreshold the threshold to use
     */
    @API
    public void setJoystickDriftZoneThreshold(@Min(0.0) @Max(0.99) double joystickDriftZoneThreshold)
    {
        if ((joystickDriftZoneThreshold < 0) || (joystickDriftZoneThreshold > 0.99))
        {
            throw new IllegalArgumentException(
                "The joystick drift zone threshold setting is out of allowed range. Should be between 0 and 0.99, inclusive. It was : " + curveFactor);
        }
        this.joystickDriftZoneThreshold = joystickDriftZoneThreshold;
    }


    @Nonnull
    @API
    protected RobotDrive getRobotDrive() { return robotDrive; }


    /**
     * Curve adjusts the input <strong>if enabled via the shouldCurveInput flag</strong>
     * to decrease sensitivity at lower speeds as described in {@link #setShouldCurveInput(boolean)}
     * and {@link #setCurveFactor(int)}.
     *
     * @param value the value to adjust
     *
     * @return the adjusted value.
     */
    @API
    protected double curveAdjust(double value)
    {
        if (!shouldCurveInput || (curveFactor == 1))
        {
            return value;

        }
        final boolean wasNegative = value == -Math.abs(value);
        final double curveValue = Math.abs(Math.pow(value, curveFactor));
        return wasNegative ? -curveValue : curveValue;
    }


    @SuppressWarnings("CallToSimpleGetterFromWithinClass")
    protected void writeEncoderValuesToSmartDash()
    {
        if (getLeftEncoder() != null)
        {
            SmartDashboard.putString("L  Encoder: ", MathUtils.formatNumber(getLeftEncoder().getDistance(), 3));
            SmartDashboard.putString("L  Count: ", MathUtils.formatNumber(getLeftEncoder().getRaw(), 0));
        }
        if (getRightEncoder() != null)
        {
            SmartDashboard.putString("R  Encoder: ", MathUtils.formatNumber(getRightEncoder().getDistance(), 3));
            SmartDashboard.putString("R  Count: ", MathUtils.formatNumber(getRightEncoder().getRaw(), 0));
        }
    }
}
