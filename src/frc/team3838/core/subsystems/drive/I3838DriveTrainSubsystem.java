package frc.team3838.core.subsystems.drive;

import frc.team3838.core.meta.DoesNotThrowExceptions;
import frc.team3838.core.subsystems.I3838Subsystem;



public interface I3838DriveTrainSubsystem extends I3838Subsystem
{

    @DoesNotThrowExceptions
    void disableMotorSafetyForAutonomousMode();

    @DoesNotThrowExceptions
    void enableMotorSafetyPostAutonomousMode();

    @DoesNotThrowExceptions
    void stop();

    void drive();
}
