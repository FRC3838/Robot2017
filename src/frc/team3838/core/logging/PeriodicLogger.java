package frc.team3838.core.logging;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

import frc.team3838.core.config.time.TimeSetting;
import frc.team3838.core.meta.API;



/**
 * A log, for use in tight loops or frequently called methods, 
 * that only logs a message at a fixed interval. 
 */
@API
public class PeriodicLogger implements Logger
{
    private final Logger logger;

    private final long intervalMs;
    
    private long lastLogTime = 0;


    @API
    public PeriodicLogger(Logger logger, long periodicIntervalDuration, @Nonnull TimeUnit periodicIntervalTimeUnit)
    {
        this.logger = logger;
        this.intervalMs = periodicIntervalTimeUnit.toMillis(periodicIntervalDuration);
    }


    @API
    public PeriodicLogger(Logger logger, @Nonnull TimeSetting periodicInterval)
    {
        this.logger = logger;
        this.intervalMs = periodicInterval.toMillis();
    }


    @API
    public PeriodicLogger(String loggerName, long periodicIntervalDuration, @Nonnull TimeUnit periodicIntervalTimeUnit)
    {
        this.logger = LoggerFactory.getLogger(loggerName);
        this.intervalMs = periodicIntervalTimeUnit.toMillis(periodicIntervalDuration);
    }


    @API
    public PeriodicLogger(String loggerName, @Nonnull TimeSetting periodicInterval)
    {
        this.logger = LoggerFactory.getLogger(loggerName);
        this.intervalMs = periodicInterval.toMillis();
    }


    @API
    public PeriodicLogger(Class loggerName, long periodicIntervalDuration, @Nonnull TimeUnit periodicIntervalTimeUnit)
    {
        this.logger = LoggerFactory.getLogger(loggerName);
        this.intervalMs = periodicIntervalTimeUnit.toMillis(periodicIntervalDuration);
    }


    @API
    public PeriodicLogger(Class loggerName, @Nonnull TimeSetting periodicInterval)
    {
        this.logger = LoggerFactory.getLogger(loggerName);
        this.intervalMs = periodicInterval.toMillis();
    }

   
    private boolean isTimeToLog()
    {
        final long currentTime = System.currentTimeMillis();
        boolean isTime = currentTime >= lastLogTime + intervalMs;
        if (isTime) { lastLogTime = currentTime; }
        return isTime;
    }
    

    @Override
    public String getName() {return logger.getName();}


    @Override
    public boolean isTraceEnabled() {return logger.isTraceEnabled();}


    @Override
    public void trace(String msg) {if (logger.isTraceEnabled() && isTimeToLog()) logger.trace (msg);}


    @Override
    public void trace(String format, Object arg) {if (logger.isTraceEnabled() && isTimeToLog()) logger.trace (format, arg);}


    @Override
    public void trace(String format, Object arg1, Object arg2) {if (logger.isTraceEnabled() && isTimeToLog()) logger.trace (format, arg1, arg2);}


    @Override
    public void trace(String format, Object... arguments) {if (logger.isTraceEnabled() && isTimeToLog()) logger.trace (format, arguments);}


    @Override
    public void trace(String msg, Throwable t) {if (logger.isTraceEnabled() && isTimeToLog()) logger.trace (msg, t);}


    @Override
    public boolean isTraceEnabled(Marker marker) {return logger.isTraceEnabled(marker);}


    @Override
    public void trace(Marker marker, String msg) {if (logger.isTraceEnabled() && isTimeToLog()) logger.trace (marker, msg);}


    @Override
    public void trace(Marker marker, String format, Object arg) {if (logger.isTraceEnabled() && isTimeToLog()) logger.trace (marker, format, arg);}


    @Override
    public void trace(Marker marker, String format, Object arg1, Object arg2) {if (logger.isTraceEnabled() && isTimeToLog()) logger.trace (marker, format, arg1, arg2);}


    @Override
    public void trace(Marker marker, String format, Object... argArray) {if (logger.isTraceEnabled() && isTimeToLog()) logger.trace (marker, format, argArray);}


    @Override
    public void trace(Marker marker, String msg, Throwable t) {if (logger.isTraceEnabled() && isTimeToLog()) logger.trace (marker, msg, t);}


    @Override
    public boolean isDebugEnabled() {return logger.isDebugEnabled();}


    @Override
    public void debug(String msg) {if (logger.isDebugEnabled() && isTimeToLog()) logger.debug (msg);}


    @Override
    public void debug(String format, Object arg) {if (logger.isDebugEnabled() && isTimeToLog()) logger.debug (format, arg);}


    @Override
    public void debug(String format, Object arg1, Object arg2) {if (logger.isDebugEnabled() && isTimeToLog()) logger.debug (format, arg1, arg2);}


    @Override
    public void debug(String format, Object... arguments) {if (logger.isDebugEnabled() && isTimeToLog()) logger.debug (format, arguments);}


    @Override
    public void debug(String msg, Throwable t) {if (logger.isDebugEnabled() && isTimeToLog()) logger.debug (msg, t);}


    @Override
    public boolean isDebugEnabled(Marker marker) {return logger.isDebugEnabled(marker);}


    @Override
    public void debug(Marker marker, String msg) {if (logger.isDebugEnabled() && isTimeToLog()) logger.debug (marker, msg);}


    @Override
    public void debug(Marker marker, String format, Object arg) {if (logger.isDebugEnabled() && isTimeToLog()) logger.debug (marker, format, arg);}


    @Override
    public void debug(Marker marker, String format, Object arg1, Object arg2) {if (logger.isDebugEnabled() && isTimeToLog()) logger.debug (marker, format, arg1, arg2);}


    @Override
    public void debug(Marker marker, String format, Object... arguments) {if (logger.isDebugEnabled() && isTimeToLog()) logger.debug (marker, format, arguments);}


    @Override
    public void debug(Marker marker, String msg, Throwable t) {if (logger.isDebugEnabled() && isTimeToLog()) logger.debug (marker, msg, t);}


    @Override
    public boolean isInfoEnabled() {return logger.isInfoEnabled();}


    @Override
    public void info(String msg) {if (logger.isInfoEnabled() && isTimeToLog()) logger.info (msg);}


    @Override
    public void info(String format, Object arg) {if (logger.isInfoEnabled() && isTimeToLog()) logger.info (format, arg);}


    @Override
    public void info(String format, Object arg1, Object arg2) {if (logger.isInfoEnabled() && isTimeToLog()) logger.info (format, arg1, arg2);}


    @Override
    public void info(String format, Object... arguments) {if (logger.isInfoEnabled() && isTimeToLog()) logger.info (format, arguments);}


    @Override
    public void info(String msg, Throwable t) {if (logger.isInfoEnabled() && isTimeToLog()) logger.info (msg, t);}


    @Override
    public boolean isInfoEnabled(Marker marker) {return logger.isInfoEnabled(marker);}


    @Override
    public void info(Marker marker, String msg) {if (logger.isInfoEnabled() && isTimeToLog()) logger.info (marker, msg);}


    @Override
    public void info(Marker marker, String format, Object arg) {if (logger.isInfoEnabled() && isTimeToLog()) logger.info (marker, format, arg);}


    @Override
    public void info(Marker marker, String format, Object arg1, Object arg2) {if (logger.isInfoEnabled() && isTimeToLog()) logger.info (marker, format, arg1, arg2);}


    @Override
    public void info(Marker marker, String format, Object... arguments) {if (logger.isInfoEnabled() && isTimeToLog()) logger.info (marker, format, arguments);}


    @Override
    public void info(Marker marker, String msg, Throwable t) {if (logger.isInfoEnabled() && isTimeToLog()) logger.info (marker, msg, t);}


    @Override
    public boolean isWarnEnabled() {return logger.isWarnEnabled();}


    @Override
    public void warn(String msg) {if (logger.isWarnEnabled() && isTimeToLog()) logger.warn (msg);}


    @Override
    public void warn(String format, Object arg) {if (logger.isWarnEnabled() && isTimeToLog()) logger.warn (format, arg);}


    @Override
    public void warn(String format, Object... arguments) {if (logger.isWarnEnabled() && isTimeToLog()) logger.warn (format, arguments);}


    @Override
    public void warn(String format, Object arg1, Object arg2) {if (logger.isWarnEnabled() && isTimeToLog()) logger.warn (format, arg1, arg2);}


    @Override
    public void warn(String msg, Throwable t) {if (logger.isWarnEnabled() && isTimeToLog()) logger.warn (msg, t);}


    @Override
    public boolean isWarnEnabled(Marker marker) {return logger.isWarnEnabled(marker);}


    @Override
    public void warn(Marker marker, String msg) {if (logger.isWarnEnabled() && isTimeToLog()) logger.warn (marker, msg);}


    @Override
    public void warn(Marker marker, String format, Object arg) {if (logger.isWarnEnabled() && isTimeToLog()) logger.warn (marker, format, arg);}


    @Override
    public void warn(Marker marker, String format, Object arg1, Object arg2) {if (logger.isWarnEnabled() && isTimeToLog()) logger.warn (marker, format, arg1, arg2);}


    @Override
    public void warn(Marker marker, String format, Object... arguments) {if (logger.isWarnEnabled() && isTimeToLog()) logger.warn (marker, format, arguments);}


    @Override
    public void warn(Marker marker, String msg, Throwable t) {if (logger.isWarnEnabled() && isTimeToLog()) logger.warn (marker, msg, t);}


    @Override
    public boolean isErrorEnabled() {return logger.isErrorEnabled();}


    @Override
    public void error(String msg) {if (logger.isErrorEnabled() && isTimeToLog()) logger.error (msg);}


    @Override
    public void error(String format, Object arg) {if (logger.isErrorEnabled() && isTimeToLog()) logger.error (format, arg);}


    @Override
    public void error(String format, Object arg1, Object arg2) {if (logger.isErrorEnabled() && isTimeToLog()) logger.error (format, arg1, arg2);}


    @Override
    public void error(String format, Object... arguments) {if (logger.isErrorEnabled() && isTimeToLog()) logger.error (format, arguments);}


    @Override
    public void error(String msg, Throwable t) {if (logger.isErrorEnabled() && isTimeToLog()) logger.error (msg, t);}


    @Override
    public boolean isErrorEnabled(Marker marker) {return logger.isErrorEnabled(marker);}


    @Override
    public void error(Marker marker, String msg) {if (logger.isErrorEnabled() && isTimeToLog()) logger.error (marker, msg);}


    @Override
    public void error(Marker marker, String format, Object arg) {if (logger.isErrorEnabled() && isTimeToLog()) logger.error (marker, format, arg);}


    @Override
    public void error(Marker marker, String format, Object arg1, Object arg2) {if (logger.isErrorEnabled() && isTimeToLog()) logger.error (marker, format, arg1, arg2);}


    @Override
    public void error(Marker marker, String format, Object... arguments) {if (logger.isErrorEnabled() && isTimeToLog()) logger.error (marker, format, arguments);}


    @Override
    public void error(Marker marker, String msg, Throwable t) {if (logger.isErrorEnabled() && isTimeToLog()) logger.error (marker, msg, t);}

}
