package frc.team3838.core.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class LogTester
{
    private static final Logger logger = LoggerFactory.getLogger(LogTester.class);


    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void log()
    {
        if (logger.isWarnEnabled())
        {
            System.out.println("Testing Logging Output...");
        }
        logger.trace("===SAMPLE TRACE MESSAGE===");
        logger.debug("===SAMPLE DEBUG MESSAGE===");
        logger.info("===SAMPLE INFO  MESSAGE===");
        logger.warn("===SAMPLE WARN  MESSAGE===");
        logger.error("===SAMPLE ERROR MESSAGE===");
        if (logger.isWarnEnabled())
        {
            System.out.println("...End Logging Test Output");
        }
    }
}
