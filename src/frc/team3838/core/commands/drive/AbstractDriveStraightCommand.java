package frc.team3838.core.commands.drive;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.ImmutableSet;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.Abstract3838Command;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection;
import frc.team3838.core.subsystems.Abstract3838NavxSubsystem;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.utils.MathUtils;



@SuppressWarnings("CallToSimpleGetterFromWithinClass")
@API
public abstract class AbstractDriveStraightCommand extends Abstract3838Command
{

    private static final String SDB_NAME_P = "DriveStraight-P";
    private static final String SDB_NAME_OFFSET_ANGLE = "DriveStraight-Offset";
    private static final String SDB_NAME_CURRENT_ANGLE = "DriveStraight-CurrentAngle";
    public static double defaultSpeedSetting = 0.35;


    private static final double DEFAULT_P = 0.027;
    private double p  = DEFAULT_P;



    protected RobotDirection robotDirection = RobotDirection.Forward;
    protected double speed = defaultSpeedSetting;

    protected boolean finished = true;

    private double driveStraightModeLastWarning = 0;
    private double lastCommandCannotRunLogTime = 0;
    private double lastCurrentAngleLogTime = 0;

    private final boolean enableTuning;


    @API
    protected AbstractDriveStraightCommand()
    {
        this.enableTuning = false;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(String name)
    {
        super(name);
        this.enableTuning = false;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(double timeout)
    {
        super(timeout);
        this.enableTuning = false;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(String name, double timeout)
    {
        super(name, timeout);
        this.enableTuning = false;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(RobotDirection robotDirection, double speed)
    {
        this.robotDirection = robotDirection;
        this.speed = speed;
        this.enableTuning = false;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(String name, RobotDirection robotDirection, double speed)
    {
        super(name);
        this.robotDirection = robotDirection;
        this.speed = speed;
        this.enableTuning = false;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(double timeout, RobotDirection robotDirection, double speed)
    {
        super(timeout);
        this.robotDirection = robotDirection;
        this.speed = speed;
        this.enableTuning = false;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(String name, double timeout, RobotDirection robotDirection, double speed)
    {
        super(name, timeout);
        this.robotDirection = robotDirection;
        this.speed = speed;
        this.enableTuning = false;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(boolean enableTuning)
    {
        this.enableTuning = enableTuning;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(String name, boolean enableTuning)
    {
        super(name);
        this.enableTuning = enableTuning;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(double timeout, boolean enableTuning)
    {
        super(timeout);
        this.enableTuning = enableTuning;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(String name, double timeout, boolean enableTuning)
    {
        super(name, timeout);
        this.enableTuning = enableTuning;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(RobotDirection robotDirection, double speed, boolean enableTuning)
    {
        this.robotDirection = robotDirection;
        this.speed = speed;
        this.enableTuning = enableTuning;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(String name, RobotDirection robotDirection, double speed, boolean enableTuning)
    {
        super(name);
        this.robotDirection = robotDirection;
        this.speed = speed;
        this.enableTuning = enableTuning;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(double timeout, RobotDirection robotDirection, double speed, boolean enableTuning)
    {
        super(timeout);
        this.robotDirection = robotDirection;
        this.speed = speed;
        this.enableTuning = enableTuning;
        initTuning();
    }


    @API
    protected AbstractDriveStraightCommand(String name, double timeout, RobotDirection robotDirection, double speed, boolean enableTuning)
    {
        super(name, timeout);
        this.robotDirection = robotDirection;
        this.speed = speed;
        this.enableTuning = enableTuning;
        initTuning();
    }


    @Nonnull
    protected abstract Abstract3838DriveTrainSubsystem getDriveTrainSubsystem();


    @Nonnull
    protected abstract Abstract3838NavxSubsystem getNavxSubsystem();


    protected boolean shouldApplyCorrection()
    {
        return true;
    }

    protected void initTuning()
    {
        if (enableTuning)
        {
            logger.info("Drive Straight Tuning is enabled.");
            double currentP;
            try
            {
                currentP = SmartDashboard.getNumber(SDB_NAME_P, -100);
            }
            catch (Exception ignore)
            {
                currentP = -100;
            }


            if (currentP == -100)
            {
                logger.debug("Placing initial tuning value on Smartdashboard");
                // We need to initialize the smart dashboard as the value is not currently there
                SmartDashboard.putNumber(SDB_NAME_P, getP());
                SmartDashboard.putNumber(SDB_NAME_OFFSET_ANGLE, 0);
            }
            else
            {
                SmartDashboard.putNumber(SDB_NAME_P, currentP);
                SmartDashboard.putNumber(SDB_NAME_OFFSET_ANGLE, SmartDashboard.getNumber(SDB_NAME_OFFSET_ANGLE, 0));
            }
        }
        else
        {
            logger.debug("Drive Straight Tuning is NOT enabled.");

        }
    }

    @Nonnull
    @Override
    protected Set<I3838Subsystem> getRequiredSubsystems()
    {
        return ImmutableSet.of(getDriveTrainSubsystem(), getDriveTrainSubsystem());
    }


    @API
    public void setRobotDirection(RobotDirection robotDirection) { this.robotDirection = robotDirection; }

    @API
    public void setSpeed(double speed) { this.speed = speed; }


    public double getSpeed() { return speed; }


    /**
     * The initialize method is called just before the first time
     * this Command is run after being started. For example, if
     * a button is pushed to trigger this command, this method is
     * called one time after the button is pushed. Then the execute
     * command is called repeated until isFinished returns true,
     * or interrupted is called by the command scheduler/runner.
     * After isFinished returns true, end is called one time
     * in order to do any cleanup or set and values.
     */
    @Override
    protected void initializeImpl() throws Exception
    {
        //noinspection ConstantConditions
        if (canCommandRun())
        {
            if (enableTuning)
            {
                //noinspection CallToSimpleSetterFromWithinClass
                setP(SmartDashboard.getNumber(SDB_NAME_P, DEFAULT_P));
                double adjAngle = SmartDashboard.getNumber(SDB_NAME_OFFSET_ANGLE, 0);
                //noinspection ConstantConditions - canCommandRun() checks for null navx
                getNavxSubsystem().getNavx().reset();
                getNavxSubsystem().getNavx().setAngleAdjustment(adjAngle);
            }

            //noinspection ConstantConditions - canCommandRun() checks for null navx
            getNavxSubsystem().getNavx().reset();
            logger.info("Using P of {} and adjustment angle of {} for {}", getP(), getNavxSubsystem().getNavx().getAngleAdjustment(), getName());
            finished = false;
        }
        else
        {
            logger.warn("COMMAND DISABLED: The Navx is not fully enabled. The Drive Straight Command cannot, and will not, run.");
            finished = true;
        }
    }


    /**
     * The execute method is called repeatedly when this Command is
     * scheduled to run until. It is called repeatedly until
     * the either isFinish returns true, interrupted is called,
     * or the command is canceled. Note that the initialize
     * method is called one time before execute is called the
     * first time. So do any setup work in the initialize
     * method. This method should run quickly. It should not
     * block for any period of time.
     */
    @SuppressWarnings("CallToSimpleGetterFromWithinClass")
    @Override
    protected void executeImpl() throws Exception
    {
        driveStraight();
    }



    protected void driveStraight()
    {
        // based on code sample at http://wpilib.screenstepslive.com/s/4485/m/13809/l/599713-gyros-measuring-rotation-and-controlling-robot-driving-direction
        try
        {
            double speed = getSpeedSetting();

            logger.trace("AbstractDriveStraightCommand.executeImpl() called");
            if (canCommandRun())
            {
                @Nullable
                final AHRS navx = getNavxSubsystem().getNavx();
                if (navx != null)
                {
                    final double currentAngle = navx.getAngle();

                    if (enableTuning)
                    {
                        final String currentAngleFormatted = MathUtils.formatNumber(currentAngle, 3);
                        SmartDashboard.putString(SDB_NAME_CURRENT_ANGLE, currentAngleFormatted);

                        // logging on each loop through (approximately every 20ms) is *way* too verbose. So we limit its frequency
                        if (logger.isDebugEnabled() && System.currentTimeMillis() > (lastCurrentAngleLogTime + 250 /* ms */))
                        {
                            logger.debug("Current Angle = {}", currentAngleFormatted);
                            lastCurrentAngleLogTime = System.currentTimeMillis();
                        }
                    }

                    // REMEMBER the joysticks are negative for forward, and positive for reverse
                    if (robotDirection == RobotDirection.Forward)
                    {
                        speed = -speed;
                    }

                    double adjustment = shouldApplyCorrection() ? (-currentAngle ) * getP() : 0 ;
                    getDriveTrainSubsystem().driveRobotViaArcadeControlRaw(adjustment, speed);
                }
                else
                {
                    // logging on each loop through (approximately every 20ms) is *way* too verbose. So we limit its frequency
                    if (System.currentTimeMillis() > (lastCommandCannotRunLogTime + TimeUnit.MINUTES.toMillis(1)))
                    {
                        logger.warn("Navx is not enable. Drive Straight mode will not function.");
                        lastCommandCannotRunLogTime = System.currentTimeMillis();
                    }
                }
            }
        }
        catch (Exception e)
        {
            // logging on each loop through (approximately every 20ms) is *way* too verbose. So we limit its frequency
            if (System.currentTimeMillis() > (driveStraightModeLastWarning + TimeUnit.MINUTES.toMillis(1)))
            {
                logger.warn("An Exception occurred in ultraSimpleDriveStraightMode(). Cause Summary: {}", e.toString(), e);
                driveStraightModeLastWarning = System.currentTimeMillis();
            }

        }
    }


    private boolean canCommandRun()
    {
        //noinspection ConstantConditions
        return getNavxSubsystem() != null
               && getNavxSubsystem().getNavx() != null
               && getDriveTrainSubsystem() != null
               && areAllSubsystemsAreEnabled();
    }


    protected double getSpeedSetting()
    {
        return speed;
    }

    @SuppressWarnings("MethodMayBeStatic")
    protected double getP()
    {
        return p;
    }


    protected void setP(double p)
    {
        this.p = p;
    }


//    protected double getOffsetAngle()
//    {
//        return offsetAngle;
//    }
//
//
//    public void setOffsetAngle(double offsetAngle)
//    {
//        this.offsetAngle = offsetAngle;
//    }


    /**
     * <p>
     * Returns whether this command is finished. If it is, then the command will be removed and
     * {@link #end()} will be called.
     * </p><p>
     * It may be useful for a team to reference the {@link #isTimedOut()}
     * method for time-sensitive commands.
     * </p><p>
     * Returning false will result in the command never ending automatically. It may still be
     * cancelled manually or interrupted by another command. Returning true will result in the
     * command executing once and finishing immediately. It is recommended to use
     * {@link edu.wpi.first.wpilibj.command.InstantCommand} (added in 2017) for this.
     * </p>
     *
     * @return whether this command is finished.
     *
     * @see #isTimedOut() isTimedOut()
     */
    @Override
    protected boolean isFinishedImpl() throws Exception
    {
        return canCommandRun() && finished;
    }


    public void setFinished(boolean finished)
    {
        this.finished = finished;
    }


    /**
     * Called once when the command ended peacefully; that is it is called once
     * after {@link #isFinished()} returns true. This is where you may want to
     * wrap up loose ends, like shutting off a motor that was being used in the
     * command.
     */
    protected void endImpl() throws Exception
    {
        getDriveTrainSubsystem().stop();
    }


    /**
     * <p>
     * Called when the command ends because somebody called {@link #cancel()} or
     * another command shared the same requirements as this one, and booted it out. For example,
     * it is called when another command which requires one or more of the same
     * subsystems is scheduled to run.
     * </p><p>
     * This is where you may want to wrap up loose ends, like shutting off a motor that was being
     * used in the command.
     * </p><p>
     * Generally, it is useful to simply call the {@link #end()} method within this
     * method, as done here.
     * </p>
     */
    @SuppressWarnings("MethodDoesntCallSuperMethod")
    protected void interruptedImpl() throws Exception
    {

    }
}
