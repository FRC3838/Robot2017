package frc.team3838.core.commands.common;

import java.lang.reflect.Field;
import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc.team3838.core.commands.Abstract3838NoSubsystemsCommand;



/**
 * A command that can be used (during development) to test a button, mostly to ensure
 * you have the correct button mapped. It eil log a message when this command is
 * executed.
 */
public class ButtonTesterCommand extends Abstract3838NoSubsystemsCommand
{
    protected final String buttonName;
    protected final String activity;


    /**
     * 
     * @param buttonName the name of the button being tested
     * @param activity the activity , such as 'pushed', 'released', etc.
     */
    protected ButtonTesterCommand(@Nonnull String buttonName, @Nonnull String activity)
    {
        this.buttonName = buttonName;
        this.activity = activity;
    }


    /**
     * 
     * @param joystickButton the button being tested
     * @param activity the activity , such as 'pushed', 'released', etc.
     */
    protected ButtonTesterCommand(@Nonnull JoystickButton joystickButton, @Nonnull String activity)
    {
        this.activity = activity;
        String joystickPort;
        String buttonNumber;

        try
        {
            final Field field = JoystickButton.class.getDeclaredField("m_joystick");
            field.setAccessible(true);
            final GenericHID genericHid = (GenericHID) field.get(joystickButton);
            final int port = genericHid.getPort();
            joystickPort = Integer.toString(port);

        }
        catch (Exception e)
        {
            logger.debug("Could not reflectively determine the joystick port. Cause Summary: {}", e.toString(), e);
            joystickPort = "<undetermined>";
        }

        try
        {
            final Field field = JoystickButton.class.getDeclaredField("m_buttonNumber");
            field.setAccessible(true);
            buttonNumber = Integer.toString((Integer) field.get(joystickButton));
        }
        catch (Exception e)
        {
            logger.debug("Could not reflectively determine the joystick button number. Cause Summary: {}", e.toString(), e);
            buttonNumber = "<undetermined>";
        }

        buttonName = "Button # " + buttonNumber + " on the joystick on port " + joystickPort;
    }
    

    @Override
    protected void executeImpl() throws Exception
    {
        logger.info("Button '{}' test. Activity: {}", buttonName, activity);
    }
}
